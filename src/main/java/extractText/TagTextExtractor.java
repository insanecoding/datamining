package extractText;

import dbUtils.DbUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.LanguageDetect;

import java.util.*;

public class TagTextExtractor extends AbstractTextExtractor {

    // tag names are mapped to the table names
    private Map <String, String> tagTableMap;

    public TagTextExtractor(DbUtils dbUtils, LanguageDetect languageDetect) {
        super(dbUtils, languageDetect);
        tagTableMap = new LinkedHashMap<>();
        initMap();
    }

    private void initMap () {
        tagTableMap.put("title", "text_from_title");
        tagTableMap.put("meta_description", "text_meta_description");
        tagTableMap.put("meta_keywords", "text_meta_keywords");
        tagTableMap.put("h1", "text_h1");
        tagTableMap.put("h2", "text_h2");
        tagTableMap.put("h3", "text_h3");
        tagTableMap.put("b", "text_b");
        tagTableMap.put("u", "text_u");
        tagTableMap.put("img", "text_img");
        tagTableMap.put("a", "text_a");
    }

    @Override
    void checkTables() {
        if (!dbUtils.tableExists("html_data") &&
                !dbUtils.checkColumns("html_data", "website_id", "html_contents", "added_at_datetime")) {
            System.out.println("Input table with websites not found");
            System.exit(-1);
        }

        // for each table
        tagTableMap.values().forEach(this::createTable);
    }

    private void createTable(String table) {
        if (dbUtils.tableExists(table)) {
            System.out.println("output table found");
        } else {
            String createQuery = "CREATE TABLE !!table!! " +
                    "( website_id INT NOT NULL UNIQUE REFERENCES list_of_websites (id) , " +
                    "text_from_tag_contents text NOT NULL," +
                    "lang VARCHAR(10) NOT NULL) ";
            createQuery = createQuery.replace("!!table!!", table);
            dbUtils.query(createQuery);
            System.out.println("output table created");
        }
    }

    private void insertToDb(int id, String text, String tableName) {
        String lang = languageDetect.detectLang(text);
        String insertQuery =
                "INSERT INTO !!table!! ( website_id, text_from_tag_contents, lang)  " +
                        "VALUES (?,?,?)";
        insertQuery = insertQuery.replace("!!table!!", tableName);
        dbUtils.query(insertQuery, id, text, lang);
    }

    private String chooseTableByTag(String tag) {
        return tagTableMap.get(tag);
    }

    @Override
    void processWebsite(int id, String htmlContents) {

        Document doc = Jsoup.parse(htmlContents);
        Map<String, String> tags = new LinkedHashMap<>();

        tags = processFirstGroupTags(doc, tags);
        tags = processSecondGroupTags(doc, tags);

        tags.forEach((tag, text) -> {
            if (!text.equals("") && !text.equals(" ")) {
                String table = chooseTableByTag(tag);
                insertToDb(id, text, table);
            }
        });
    }

    private String cleanText(String text){
        // delete everything except letters and digits
        text = text.replaceAll("[^A-Za-z0-9 ]", " ").toLowerCase();
        // delete odd whitespaces
        text = text.replaceAll("\\s+", " ");
        return text;
    }

    private Map<String, String> processSecondGroupTags(Document doc, Map<String, String> tags) {
        // now process other tags with text
        // list of target tags
        List<String> extraTags = Arrays.asList("h1", "h2", "h3", "b", "u", "img", "a");

        // create string representation of a list (to use with <code>doc.select()</code>)
        String str = extraTags.toString();
        String selectedTags = str.substring(1, str.length() - 1);

        // initialize with empty value
        extraTags.forEach(chosenTag -> tags.put(chosenTag, ""));

        Elements elements = doc.select(selectedTags);
        elements.forEach(element -> {
            // case 1: <img> tag - has alt-attribute
            if (element.tagName().equals("img") && element.hasAttr("alt")) {
                String value = element.attr("alt");
                value = cleanText(value);
                String totalCollected = tags.get("img") + " " + value;
                totalCollected = cleanText(totalCollected);
                tags.put("img", totalCollected);
            }
            // case 2: <a> tag - has title-attribute or text between <a> </a>
            if (element.tagName().equals("a")) {
                String result = "";
                // if has title-attribute
                if (element.hasAttr("title")) {
                    String value = element.attr("title");
                    value = cleanText(value);
                    result += " " + value;
                }
                // for text between <a> </a>
                if (element.hasText()) {
                    String value = element.text();
                    value = cleanText(value);
                    result += " " + value;
                }

                if (!result.equals("")) {
                    String collected = tags.get("a") + " " + result;
                    collected = cleanText(collected);
                    tags.put("a", collected);
                }
            }

            // case 3: <h1> - <h3>
            if (element.hasText()) {
                String result = element.text();
                result = cleanText(result);

                String elemName = element.tagName();
                String collected = tags.get(elemName) + " " + result;
                collected = cleanText(collected);
                tags.put(elemName, collected);
            }
        });
        return tags;
    }

    private Map<String, String> processFirstGroupTags(Document doc, Map<String, String> tags) {
        // initialize map with empty starting values (to avoid nulls)
        tags.put("title", "");
        tags.put("meta_description", "");
        tags.put("meta_keywords", "");

        // process title tag
        String title = doc.title();
        if (!title.equals("")) {
            String titleText = cleanText(title);
            tags.put("title", titleText);
        }

        // process 2 kinds of meta
        for (Element meta : doc.select("meta")) {
            if (meta.attr("name").compareToIgnoreCase("description") == 0 &&
                    !meta.attr("content").equals("")) {
                String metaDescriptionText = meta.attr("content");
                metaDescriptionText = cleanText(metaDescriptionText);
                tags.put("meta_description", metaDescriptionText);
            }

            if (meta.attr("name").compareToIgnoreCase("keywords") == 0 &&
                    !meta.attr("content").equals("")) {
                // delete everything except letters and digits
                String metaKeywordsText = meta.attr("content");
                metaKeywordsText = cleanText(metaKeywordsText);
                tags.put("meta_keywords", metaKeywordsText);
            }
        }
        return tags;
    }
}
