package extractText;

import dbUtils.DbUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import utils.LanguageDetect;

public class MainTextExtractor extends AbstractTextExtractor{

    public MainTextExtractor(DbUtils dbUtils, LanguageDetect languageDetect) {
        super(dbUtils, languageDetect);
    }

    @Override
    void checkTables() {
        if (!dbUtils.tableExists("html_data") &&
                !dbUtils.checkColumns("html_data", "website_id", "html_contents", "added_at_datetime")) {
            System.out.println("Input table with websites not found");
            System.exit(-1);
        }

        if (dbUtils.tableExists("text_data")) {
            System.out.println("output table found");
        } else {
            String createQuery = "CREATE TABLE text_data " +
                    "( website_id INT NOT NULL UNIQUE REFERENCES list_of_websites (id) , " +
                    "text_contents text NOT NULL," +
                    "lang VARCHAR(10) NOT NULL, text_length int) ";
            dbUtils.query(createQuery);
            System.out.println("output table created");
        }
    }

    @Override
    void processWebsite(int id, String htmlContents) {
        Document doc = Jsoup.parse(htmlContents);

        if (doc.body() != null) {
            String textInPage = doc.body().text();

            // leave only a...z, A...Z and 0...0 letters and whitespaces
            String lettersOnly = textInPage.replaceAll("[^A-Za-z0-9 ]", " ");
            // remove duplicate whitespaces
            String result  = lettersOnly.replaceAll("\\s+", " ");

            int text_length = result.length();
            String lang = languageDetect.detectLang(result);
            System.out.println("" + id + ' ' + lang);

            if (!lettersOnly.equals("")) {
                String insertQuery =
                        "INSERT INTO text_data ( website_id, text_contents, lang, text_length)  " +
                                "VALUES (?,?,?,?)";
                dbUtils.query(insertQuery, id, result, lang, text_length);
            }
        } else {
            System.out.println("<body> is null");
        }
    }
}
