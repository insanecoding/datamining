package extractText;

import dbUtils.DbUtils;
import utils.LanguageDetect;

import java.util.Collections;
import java.util.List;

abstract class AbstractTextExtractor {
    protected DbUtils dbUtils;
    protected LanguageDetect languageDetect;

    public AbstractTextExtractor(DbUtils dbUtils, LanguageDetect languageDetect) {
        this.dbUtils = dbUtils;
        this.languageDetect = languageDetect;
    }

    abstract void processWebsite(int id, String html);

    abstract void checkTables();

    public void extractText(List<String> categories) {
        // check and create tables (if necessary)
        checkTables();
        // now iterate all chosen categories
        categories.forEach(category -> {
            System.out.println("Processing category: " + category);
            List<Object[]> htmlsInCategory =
                    dbUtils.select("select hd.website_id, html_contents\n" +
                            "from html_data as hd join list_of_websites as lw on hd.website_id = lw.id\n" +
                            "join list_of_categories as lc on lw.category_id = lc.id\n" +
                            "where category_name = ?", category);
            if (htmlsInCategory.size() != 0) {
                // mix up the contents
                Collections.shuffle(htmlsInCategory);
                // now apply function to work with text
                htmlsInCategory.forEach(html -> {
                    int id = (int) html[0];
                    String htmlContents = (String) html[1];
                    processWebsite(id, htmlContents);
                });
            }
        });
    }
}
