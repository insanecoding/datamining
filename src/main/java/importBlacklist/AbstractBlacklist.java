package importBlacklist;

abstract class AbstractBlacklist {

    String blacklistName;
    String downloadedFrom;

    AbstractBlacklist(String blacklistName, String downloadedFrom) {
        this.blacklistName = blacklistName;
        this.downloadedFrom = downloadedFrom;
    }

    abstract void addBlacklist();      // will be defined for each type of blacklist
}

