package importBlacklist;

import dbUtils.DbUtils;
import utils.MiscUtils;

import java.io.*;
import java.util.List;

public class DefaultBlacklist extends AbstractBlacklist{

    private DbUtils dbUtils;
    private String importMode;
    private String pathToBL;

    public DefaultBlacklist(String blacklistName, String downloadedFrom,
                            String pathToBL, DbUtils dbUtils, String mode) {
        super(blacklistName, downloadedFrom);
        this.pathToBL = pathToBL;
        this.importMode = mode;
        this.dbUtils = dbUtils;
    }

    private void checkImportMode() {
        if (importMode.equals("rewrite")) {

            if (dbUtils.tableExists("data_sources")) {
                dbUtils.query("drop table data_sources cascade");
            }

            if (dbUtils.tableExists("list_of_categories")) {
                dbUtils.query("drop table list_of_categories cascade");
            }

            if (dbUtils.tableExists("list_of_websites")) {
                dbUtils.query("drop table list_of_websites cascade");
            }
        }
    }

    private void checkTables() {
        if (!dbUtils.tableExists("data_sources")) {
            String sql = "CREATE TABLE data_sources " +
                    "( id SERIAL NOT NULL PRIMARY KEY," +
                    "source_name varchar(150) unique NOT NULL," +
                    "url varchar(300) NOT NULL check ( length(url) < 300) )";
            dbUtils.query(sql);
            System.out.println("Created table data_sources");
        } else {
            System.out.println("data_sources already exists");
        }

        if (!dbUtils.tableExists("list_of_categories")) {
            String sql = "CREATE TABLE list_of_categories " +
                    "( id SERIAL NOT NULL PRIMARY KEY," +
                    "category_name varchar(250) NOT NULL unique, " +
                    "isused int ) ";
            dbUtils.query(sql);
            System.out.println("Created table list_of_categories");
        } else {
            System.out.println("list_of_categories already exists");
        }

        if (!dbUtils.tableExists("list_of_websites")) {
            String sql = "CREATE TABLE list_of_websites " +
                    "( id SERIAL NOT NULL PRIMARY KEY," +
                    "category_id int NOT NULL REFERENCES list_of_categories(id)," +
                    "source_id int NOT NULL REFERENCES data_sources(id)," +
                    "website_url varchar(300) NOT NULL unique check ( length(website_url) < 300))";
            dbUtils.query(sql);
            System.out.println("Created table list_of_websites");
        } else {
            System.out.println("list_of_websites already exists");
        }
    }

    private void getStatistics() {
        List<Object[]> stat = dbUtils.select("SELECT count(*) from list_of_websites");
        System.out.print("Entries in list of websites: ");
        System.out.println(stat.get(0)[0]);
//        dbUtils.printResultSet(stat);
    }

    private long[] navigateDirectory(File root, long[] totalCount) {
        File[] files = root.listFiles();
        assert files != null;

        for (File file : files) {
            if (file.isDirectory()) {
                totalCount = navigateDirectory(file, totalCount);
            } else {
                if (file.isFile() && file.getName().equals("domains")) {
                    processFile(file, totalCount);
                }
            }
        }
        return totalCount;
    }

    private void processFile(File file, long[] totalCount) {
        FileReader fileReader = null;
        BufferedReader bufReader = null;
        try {
            String category = file.getParentFile().getName();
            System.out.println(category + ": ");            // print directory's name

            dbUtils.query("INSERT into list_of_categories (category_name, isused) VALUES (?, 0)",
                    category);

            List<Object[]> catID =
                    dbUtils.select("SELECT id FROM list_of_categories " +
                            "where category_name = ? ", category);

            List<Object[]> dataSourceID =
                    dbUtils.select("SELECT id FROM data_sources " +
                            "where source_name = ? ", blacklistName);

            int category_id = (int) catID.get(0)[0];
            int source_id = (int) dataSourceID.get(0)[0];

            fileReader = new FileReader(file);
            bufReader = new BufferedReader(fileReader);

            long counter = 0;
            long errCounter = 0;

            String line;
            while ((line = bufReader.readLine()) != null) {          // while reading file
                line = MiscUtils.checkURLBeginning(line);
                if (MiscUtils.isValidURL(line)) {

                    dbUtils.query("INSERT INTO list_of_websites " +
                                    " (category_id, source_id, website_url)  VALUES (?, ?, ?)",
                            category_id, source_id, line);
                    counter++;
                } else {
                    errCounter++;
                }
            }
            totalCount[0] += counter;
            totalCount[1] += errCounter;

            System.out.println("" + counter + " strings in category.");
            System.out.println("With errors: " + errCounter);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufReader != null) {
                try {
                    bufReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void addBlacklist() {
        long startTime = System.currentTimeMillis();
        checkImportMode();
        checkTables();
        dbUtils.query("insert into data_sources (source_name, url) VALUES (?, ?)", blacklistName, downloadedFrom);
        File curDir = new File(pathToBL);

        long[] totalCnt = {0, 0};
        getStatistics();
        totalCnt = navigateDirectory(curDir, totalCnt);
        System.out.println("Total strings read = " + totalCnt[0] + ", with errors = " + totalCnt[1]);
        getStatistics();

        long endTime = System.currentTimeMillis();
        double seconds = (double) (endTime - startTime) / 1000;
        System.out.println("That took " + seconds + " seconds, " + seconds / 60 + " minutes");
    }

    DefaultBlacklist(String blacklistName, String downloadedFrom) {
        super(blacklistName, downloadedFrom);
    }
}
