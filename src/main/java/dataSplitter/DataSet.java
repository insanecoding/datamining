package dataSplitter;

import java.util.List;
import java.util.Map;

/**
 * Class to store learn and test identifiers (by category)
 */
public class DataSet {
    private Map<String, List<Integer>> learnDataSet;
    private Map <String, List<Integer>> testDataSet;

    DataSet(Map<String, List<Integer>> learnDataSet, Map<String, List<Integer>> testDataSet) {
        this.learnDataSet = learnDataSet;
        this.testDataSet = testDataSet;
    }

    public Map<String, List<Integer>> getLearnDataSet() {
        return learnDataSet;
    }

    public Map<String, List<Integer>> getTestDataSet() {
        return testDataSet;
    }
}
