package dataSplitter;

import dbUtils.DbUtils;

import java.util.*;

public class DataSetTools {

    private DbUtils dbUtils;

    public DataSetTools(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    /**
     * Create joint dataSet of IDs based on text_main
     * Its size is chosenCategories.size * sitesPerCategory
     *
     * @param chosenCategories categories used
     * @param sitesPerCategory amount of websites in each category
     *                         (no more than <code>sitesPerCategory</code>
     *                         DataSetIDs parameters:
     * @param lang             text language
     * @param minLength        min text length
     * @param maxLength        max text length
     * @return categories with website identifiers
     */
    public Map<String, List<Integer>> getIDsByCategory(List<String> chosenCategories, int sitesPerCategory,
                                                       String lang, int minLength, int maxLength) {

        Map<String, List<Integer>> IDsByCategory = new LinkedHashMap<>();

        for (String category : chosenCategories) {
            List<Integer> IDsInCategory = new LinkedList<>();
            List<Object[]> resultList = dbUtils.select
                    ("select website_id " +
                            "from text_data as td join list_of_websites as lw on td.website_id = lw.id " +
                            "join list_of_categories as lc on lw.category_id = lc.id " +
                            "where lang = ? " +
                            "and lc.category_name = ? " +
                            "and (text_length BETWEEN ? and ?)", lang, category, minLength, maxLength);

            // shuffle data to make their order random
            Collections.shuffle(resultList);

            for (int i = 0; i < resultList.size(); i++) {
                if (i < sitesPerCategory) {
                    Object[] ob = resultList.get(i);
                    int id = (int) ob[0];
                    IDsInCategory.add(id);
                }
            }
            IDsByCategory.put(category, IDsInCategory);
        }

        return IDsByCategory;
    }

    /**
     * Split dataSet into 2 parts: learn and test
     *
     * @param IDsByCategory  dataSet of categories with mapped identifiers
     * @param partitionLearn learn dataSet ratio
     * @return class <code>DataSetIDs</code> with learn and test dataSets (categories and mapped websiteIDs)
     */
    public DataSet dataSplitter(Map<String, List<Integer>> IDsByCategory, double partitionLearn) {

        Map<String, List<Integer>> learnDataSet = new LinkedHashMap<>();
        Map<String, List<Integer>> testDataSet = new LinkedHashMap<>();

        IDsByCategory.forEach((category, listOfIDs) -> {
            Collections.shuffle(listOfIDs);
            // size of list mapped to category
            int totalSize = listOfIDs.size();
            // amount of websites for learning (in this category)
            int learningIDsNumber = (int) (totalSize * partitionLearn);
            // do actual split
            List<Integer> learningIDs = listOfIDs.subList(0, learningIDsNumber);
            List<Integer> testingIDs = listOfIDs.subList(learningIDsNumber, totalSize);
            // write data
            learnDataSet.put(category, learningIDs);
            testDataSet.put(category, testingIDs);
        });
        return new DataSet(learnDataSet, testDataSet);
    }

    /**
     * For this dataSet with IDs, create dataSet with textData
     *
     * @param IDsByCategory dataSet with IDs
     * @return real text contents and list of unknowns IDs
     */
    public Map<String, List<String>> getTextsByCategory(Map<String, List<Integer>> IDsByCategory,
                                                        String tableName, String fieldName) {

        // will contain necessary features (they are not null)
        Map<String, List<String>> textData = new LinkedHashMap<>();

        for (String category : IDsByCategory.keySet()) {
            // list of strings: will contain the texts of websites that have desired features
            List<String> textsInCategory = new LinkedList<>();

            // list of integers: will contain the ids of websites that don't have other features except text main
            List<Integer> unknowns = new LinkedList<>();

            System.out.println("DataSetIDs for " + category);
            List<Integer> ids = IDsByCategory.get(category);

            ids.forEach(id ->
            {
                // example usage: "select text_contents from text_data where website_id = 11111"
                String sql = "select !what_toSelect! from !fromTable! where website_id = ? ";
                sql = sql.replace("!what_toSelect!", fieldName);
                sql = sql.replace("!fromTable!", tableName);

                List<Object[]> resultList = dbUtils.select(sql, id);
                if (resultList.size() != 0) {
                    String text = (String) resultList.get(0)[0];
                    textsInCategory.add(text);
                } else {  // if resultList.size == 0 then it is unknown
                    unknowns.add(id);
                }
            });
            System.out.println("Texts in category " + category + " , size: " + textsInCategory.size());
            System.out.println("Unknowns in category " + category + " , size: " + unknowns.size());
            textData.put(category, textsInCategory);
        }
        return textData;
    }

    public void createFilenames(Map<String, List<Integer>> IDsByCategory) {

        if (dbUtils.tableExists("filenames")) {
            dbUtils.query("drop table filenames");
        }
        dbUtils.query("CREATE TABLE filenames " +
                "(website_id serial not null primary key, filename text )");


        for (String category : IDsByCategory.keySet()) {
            System.out.println("Filenames for " + category);
            List<Integer> ids = IDsByCategory.get(category);

            ids.forEach(id -> {
                String sql = "select website_url, td.website_id " +
                        "from text_data as td join list_of_websites as lw on td.website_id = lw.id " +
                        "where website_id = ? ";

                List<Object[]> resultSet = dbUtils.select(sql, id);
                if (resultSet.size() != 0) {
                    String filename = "#" + resultSet.get(0)[1].toString() + '_'
                            + resultSet.get(0)[0].toString().replaceAll("[^A-Za-z0-9]", "");
                    dbUtils.query("insert into filenames (website_id, filename) " +
                            "values (?, ?)", id, filename);
                }
            });
        }
    }

    public Map<String, List<String>> createUnknownDataNames(List<String> categories, String table) {
        // Map: category -> list of filenames (for unknowns)
        Map<String, List<String>> unknownFilenames = new LinkedHashMap<>();

        categories.forEach(category -> {
            String sql = "select filename\n" +
                    "from filenames as fn left OUTER JOIN !tableName! as td on fn.website_id = td.website_id\n" +
                    "  join list_of_websites as lw on fn.website_id = lw.id\n" +
                    "  join list_of_categories as lc on lw.category_id = lc.id\n" +
                    "where td.website_id is null and lc.category_name = ?";
            sql = sql.replace("!tableName!", table);

            List<Object[]> res = dbUtils.select(sql, category);
            List<String> filenamesInCategory = new LinkedList<>();

            if (res.size() != 0) {
                res.forEach(entry -> {
                    String filename = (String) entry[0];
                    filenamesInCategory.add(filename);
                });
                unknownFilenames.put(category, filenamesInCategory);
            }
        });

        return unknownFilenames;
    }

    public Map<String, String> createValidDataNames(String category, String table, String field) {
        Map<String, String> filenamesInCategory = new LinkedHashMap<>();

        String sql = "select filename, !field! " +
                "from filenames as fn left OUTER JOIN !table! as td on fn.website_id = td.website_id\n" +
                "  join list_of_websites as lw on fn.website_id = lw.id\n" +
                "  join list_of_categories as lc on lw.category_id = lc.id\n" +
                "where td.website_id is not null and lc.category_name = ?";
        sql = sql.replace("!field!", field);
        sql = sql.replace("!table!", table);

        List<Object[]> res = dbUtils.select(sql, category);

        if (res.size() != 0) {
            res.forEach(entry -> {
                String filename = (String) entry[0];
                String text = (String) entry[1];
                filenamesInCategory.put(filename, text);
            });
        }
        return filenamesInCategory;
    }
}