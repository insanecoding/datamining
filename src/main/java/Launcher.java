import aml_dat.createDictionary.CreateDictionaryMain;
import aml_dat.makeAML.MakeAML;
import aml_dat.writeAML.AMLWriter;
import aml_dat.writeAML.DATWriter;
import dataSplitter.DataSet;
import dataSplitter.DataSetTools;
import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import downloading.BlacklistDownloader;
import downloading.HTMLDownloader;
import extractNGrams.NGramExtractor;
import extractTagStat.TagAML;
import extractTagStat.TagDAT;
import extractTagStat.TagDictionary;
import extractTagStat.TagExtractor;
import extractText.MainTextExtractor;
import extractText.TagTextExtractor;
import importBlacklist.DefaultBlacklist;
import reporting.PerformanceReader;
import schemeGenerator.SchemeGenerator;
import utils.*;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Launcher {
    private String configName = System.getProperty("user.dir") + "\\src\\config.properties";
    private DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);

    private String chosenCategories = System.getProperty("user.dir") + "\\src\\categories.txt";
    private String[] urls = MiscUtils.readPropertiesAsArray(configName, "urls");
    private String[] blacklistNames = MiscUtils.readPropertiesAsArray(configName, "blacklistNames");
    private String blacklists = MiscUtils.readProperty(configName, "blacklists");
    private String blacklistsDownloaded = blacklists + "downloads//";
    private String blacklistsUnpacked = blacklists + "unpacked//";
    private String resources = MiscUtils.readProperty(configName, "resources");
    private String experiments = MiscUtils.readProperty(configName, "experiments");

    private void downloadBlacklists() {
        String proxyIP = MiscUtils.readProperty(configName, "proxyIP");
        String port = MiscUtils.readProperty(configName, "port");
        String[] useProxy = MiscUtils.readPropertiesAsArray(configName, "useProxy");

        int blacklistsNumber = 4;
        for (int i = 0; i < blacklistsNumber; i++) {
            BlacklistDownloader blacklistDownloader;
            // if using proxy
            if (Boolean.parseBoolean(useProxy[i])) {
                blacklistDownloader = new BlacklistDownloader(Boolean.parseBoolean(useProxy[i]), proxyIP, port);
            } else {    // otherwise
                blacklistDownloader = new BlacklistDownloader(Boolean.parseBoolean(useProxy[i]));
            }
            blacklistDownloader.downloadURL(urls[i], blacklistsDownloaded + blacklistNames[i]);
        }
    }

    private void uncompressBlacklists() {
        UncompressTool.uncompressAll(blacklistsDownloaded, blacklistsUnpacked);
    }

    private void importBlacklists() {
        String[] importModes = MiscUtils.readPropertiesAsArray(configName, "importModes");
        DefaultBlacklist shallaBlacklist = new DefaultBlacklist(blacklistNames[1], urls[1],
                blacklistsUnpacked + blacklistNames[1], dbUtils, importModes[1]);
        shallaBlacklist.addBlacklist();

        DefaultBlacklist urlBlacklist = new DefaultBlacklist(blacklistNames[0], urls[0],
                blacklistsUnpacked + blacklistNames[0], dbUtils, importModes[0]);
        urlBlacklist.addBlacklist();
    }

    private void downloadHTML(List<String> categories) {
        int threadsNumber = 300;
        int sitesPerCategory = Integer.parseInt(MiscUtils.readProperty(configName, "downloadPerCategory"));

        HTMLDownloader HTMLDownloader = new HTMLDownloader(dbUtils, threadsNumber);
        HTMLDownloader.downloadHTMLs(categories, sitesPerCategory);
    }

    private List<String> chooseCategories() {
        List <String> categories = MiscUtils.readTextFileToList(chosenCategories);
        CategoryChooser categoryChooser = new CategoryChooser(dbUtils);
        List <String> result = categoryChooser.makeChoiceOnCategories(categories);
        System.out.println("Chosen categories: ");
        System.out.println(result);
        return result;
    }

    private void extractMainText(List<String> categories) {
        String profilesLong = resources + "profiles";
        LanguageDetect languageDetect = new LanguageDetect(profilesLong);
        MainTextExtractor mainTextExtractor = new MainTextExtractor(dbUtils, languageDetect);
        mainTextExtractor.extractText(categories);
        languageDetect.clear();
    }

    private void extractTextFromTags(List <String> categories) {
        String profilesShort = resources + "profiles.sm";
        LanguageDetect languageDetect = new LanguageDetect(profilesShort);
        TagTextExtractor tagTextExtractor = new TagTextExtractor(dbUtils, languageDetect);
        tagTextExtractor.extractText(categories);
        languageDetect.clear();
    }

    private Map<String, List<Integer>> createIDsByCategory(List<String> categories){
        String lang = MiscUtils.readProperty(configName, "lang");
        int minLength = Integer.parseInt(MiscUtils.readProperty(configName, "minLength"));
        int maxLength = Integer.parseInt(MiscUtils.readProperty(configName, "maxLength"));
        int textsPerCategory = Integer.parseInt(MiscUtils.readProperty(configName, "dataPerCategory"));

        DataSetTools dataSetTools = new DataSetTools(dbUtils);
        return dataSetTools.getIDsByCategory(categories, textsPerCategory,
                        lang, minLength, maxLength);
    }

    private void initialSettings(List<String> categories){
        double partitionLearn = Double.parseDouble(MiscUtils.readProperty(configName, "partitionLearn"));
        DataSetTools dataSetTools = new DataSetTools(dbUtils);

        // now categoriesNum * textPerCategory elements
        Map<String, List<Integer>> IDsByCategory = createIDsByCategory(categories);
        // save filenames for the dataSet
        dataSetTools.createFilenames(IDsByCategory);

        DataSet dataSet = dataSetTools.dataSplitter(IDsByCategory, partitionLearn);

        dbUtils.query("CREATE table learningIDs (\n" +
                "  id INTEGER NOT NULL UNIQUE,\n" +
                "  category VARCHAR(300)\n" +
                ")");

        dbUtils.query("CREATE TABLE testingIDs (\n" +
                "  id INTEGER not NULL UNIQUE,\n" +
                "  category VARCHAR(300)\n" +
                ")");

        Map <String, List<Integer>> learn = dataSet.getLearnDataSet();

        learn.forEach( (category, ids) ->
                ids.forEach(id ->
                        dbUtils.query("insert into learningIDs (id, category) values (?, ?) ", id, category)));

        Map <String, List<Integer>> test = dataSet.getTestDataSet();
        test.forEach( (category, ids) ->
                ids.forEach(id ->
                        dbUtils.query("insert into testingIDs (id, category) values (?, ?) ", id, category)));
    }

    private Map<String, List<Integer>> retrieveIDsByCategory(List<String> categories) {
        Map<String, List<Integer>> IDsByCategory = new LinkedHashMap<>();

        categories.forEach(category -> {
            String sql = "select website_id\n" +
                    "from filenames as fn join list_of_websites as lw on fn.website_id = lw.id\n" +
                    "join list_of_categories as lc on lw.category_id = lc.id\n" +
                    "where category_name = ? ";
            List<Object[]> res = dbUtils.select(sql, category);

            if (res.size() == 0) {
                System.exit(-1);
            }

            List<Integer> ids = res.stream()
                                .map(entry -> Integer.parseInt(entry[0].toString()))
                                .collect(Collectors.toCollection(LinkedList::new));
            IDsByCategory.put(category, ids);
        });
        return IDsByCategory;
    }

    private Map <String, List<Integer>> retrieveIDMap(String set){
        Map <String, List<Integer>> resultMap = new LinkedHashMap<>();
        String categoriesQuery = "select category from !set! group by category order by category";
        categoriesQuery = categoriesQuery.replace("!set!", set);

        List<Object[]> res = dbUtils.select(categoriesQuery);
        if (res.size() != 0) {

            String sql = "select id from !set! where category = ? ";
            sql = sql.replace("!set!", set);

            String finalSql = sql;
            res.forEach(entry -> {
                String category = (String) entry[0];
                List <Integer> ids;
                List<Object[]> result = dbUtils.select(finalSql, category);
                if (result.size() != 0) {
                    ids = result.stream()
                            .map(id -> Integer.parseInt(id[0].toString()))
                            .collect(Collectors.toCollection(LinkedList::new));
                    resultMap.put(category, ids);
                }
            });
        }
        return resultMap;
    }

    private void createDictionary(Map<String, List<String>> textData) {
        String stopWords = resources + "stopwords.dat";

        CreateDictionaryMain createDictionaryMain =
                new CreateDictionaryMain(dbUtils, configName, stopWords);
        createDictionaryMain.createDictionary(textData);
    }

    private void addExperiment(String experimentName) {
        dbUtils.query("insert into experiments (exp_name) values (?)", experimentName);
    }

    private void prepareAllAMLContents(List<String> categories) {
        String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");
        String path = experiments + "/aml_dat/";
        int count = Integer.parseInt(MiscUtils.readProperty(configName, "featuresByCategory"));
        DataSetTools dataSetTools = new DataSetTools(dbUtils);
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);

        for (int experimentNum = 0; experimentNum < modes.length; experimentNum++) {
            String[] parameters = MiscUtils.chooseTable(modes[experimentNum]);
            String field = parameters[0];
            String table = parameters[1];

            Map<String, List<String>> textData = dataSetTools.getTextsByCategory(IDsByCategory, table, field);
            createDictionary(textData);

            MakeAML makeAML = new MakeAML(dbUtils, modes[experimentNum], path, new int[]{count});
            makeAML.makeAML(categories, table, field, experimentNum);
        }
    }

    private void prepareAMLContents(String mode, int experimentNum, List<String> categories) {
        String path = experiments + "/amls/" + mode + "/";
        int count = Integer.parseInt(MiscUtils.readProperty(configName, "featuresByCategory"));
        DataSetTools dataSetTools = new DataSetTools(dbUtils);
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);

        String[] parameters = MiscUtils.chooseTable(mode);
        String field = parameters[0];
        String table = parameters[1];

        Map<String, List<String>> textData = dataSetTools.getTextsByCategory(IDsByCategory, table, field);
        createDictionary(textData);

        MakeAML makeAML = new MakeAML(dbUtils, mode, path, new int[]{count});
        makeAML.makeAML(categories, table, field, experimentNum);
    }

    private void writeAMLDATForAll(List<String> categories) {
        int featuresPerCategory = Integer.parseInt(MiscUtils.readProperty(configName, "featuresByCategory"));
        String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");

        Map <String, List<Integer>> learn = retrieveIDMap("learningIDs");
        Map <String, List<Integer>> test = retrieveIDMap("testingIDs");
        try {
            AMLWriter amlWriter = new AMLWriter(dbUtils, experiments + "/amls/text_all_ngram/text_all_ngram_learn.aml");
            amlWriter.createUnitedAML(categories, experiments + "/amls/text_all_ngram/text_all_ngram_learn.dat");

            amlWriter = new AMLWriter(dbUtils, experiments + "/amls/text_all_ngram/text_all_ngram_test.aml");
            amlWriter.createUnitedAML(categories, experiments + "/amls/text_all_ngram/text_all_ngram_test.dat");

            DATWriter datWriter = new DATWriter(dbUtils, "text_all_ngram",
                    experiments + "/amls/text_all_ngram/", featuresPerCategory);

            datWriter.createDATForAllExperiments(modes.length, categories.size() * 1000, learn, test);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeAMLDATForeachExperiment(List <String> categories) {
        try {
            String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");
            int featuresPerCategory = Integer.parseInt(MiscUtils.readProperty(configName,
                    "featuresByCategory"));

            Map <String, List<Integer>> learn = retrieveIDMap("learningIDs");
            Map <String, List<Integer>> test = retrieveIDMap("testingIDs");

            for (int i = 0; i < modes.length; i++) {
                System.out.println("Mode: " + modes[i]);

                AMLWriter amlWriter = new AMLWriter(dbUtils, experiments + "/amls/" + modes[i] + "_learn.aml");
                amlWriter.createExperimentAML(i, categories,
                        experiments + "/amls/" + modes[i] + "_learn.dat");

                amlWriter = new AMLWriter(dbUtils, experiments + "/amls/" + modes[i] + "_test.aml");
                amlWriter.createExperimentAML(i, categories,
                        experiments + "/amls/" + modes[i] + "_test.dat");

                DATWriter datWriter = new DATWriter(dbUtils, modes[i],
                        experiments + "/amls/", featuresPerCategory);

                datWriter.createDATForExperiment(i, categories.size() * 1000, learn, test);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeAMLDATForOneExperiment(String mode, int experimentNumber, List <String> categories) {
        try {
            int featuresPerCategory = Integer.parseInt(MiscUtils.readProperty(configName,
                    "featuresByCategory"));

            Map <String, List<Integer>> learn = retrieveIDMap("learningIDs");
            Map <String, List<Integer>> test = retrieveIDMap("testingIDs");

                System.out.println("Mode: " + mode);

                AMLWriter amlWriter = new AMLWriter(dbUtils, experiments + "/amls/" + mode +
                        "/" + mode + "_learn.aml");
                amlWriter.createExperimentAML(experimentNumber, categories,
                        experiments + "/amls/" + mode + "/" + mode + "_learn.dat");

                amlWriter = new AMLWriter(dbUtils, experiments + "/amls/" + mode +
                        "/" + mode + "_test.aml");
                amlWriter.createExperimentAML(experimentNumber, categories,
                        experiments + "/amls/" + mode +
                                "/" + mode + "_test.dat");

                DATWriter datWriter = new DATWriter(dbUtils, mode,
                        experiments + "/amls/" + mode + "/", featuresPerCategory);

                datWriter.createDATForExperiment(experimentNumber, categories.size() * 1000, learn, test);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void schemeGen(String mode, List <String> categories) {
        String experimentsRoot = MiscUtils.readProperty(configName, "experiments");
        String amls = experimentsRoot + "/amls/" + mode + "/";
        String schemes = experimentsRoot + "/schemes/" + mode + "/";
        String models = experimentsRoot + "/models/" + mode + "/";
        String performance = experimentsRoot + "/performance/" + mode + "/";

        String templates = MiscUtils.readProperty(configName, "resources") + "/templates/";
        String baseLearnersTemplate = templates + "1)base_learner_model.rmp";
        String stackingTemplate = templates + "2)stacking.rmp";
        String applyModelTemplate = templates + "3)apply_model.rmp";
        String readPerformanceTemplate = templates + "4)read_performance.rmp";

        SchemeGenerator sg = new SchemeGenerator(amls, schemes);
        sg.setBaseLearnerTemplate(baseLearnersTemplate);
        sg.setStackingTemplate(stackingTemplate);
        sg.setApplyModelTemplate(applyModelTemplate);
        sg.setReadPerformanceTemplate(readPerformanceTemplate);

        for (int i = 0; i < categories.size(); i++ ) {
            sg.generateBaseLearnersScheme(i + 1, categories.get(i),
                    amls + mode + "_learn.aml", models, performance, mode);
        }

        List<String> base_models = new LinkedList<>();
        categories.forEach(category -> {
            // getting order num of each category in list. For RM, it shouldn't start from 0 ==> increment by 1
            int orderNum = categories.indexOf(category) + 1;
            // add base model path to the list
            // the list element would be like:
            // C:/Coding/Java/Data Mining Tools/DataMiningTools/AML_DAT/text_main/Models/text_base_1.mod
            // regexp used to replace \\ into / in filepath.
            // If not doing so, the path in AML file will be invalid
            base_models.add((models + "text_base_" + orderNum + ".mod").replace("\\", "\\/"));
        });
        // now 'base_models' list is full of paths to base models

        base_models.forEach( e -> e.replace("\\", File.separator));

        sg.generateStackingScheme(amls + mode + "_learn.aml", models, performance, base_models, 0.0001, 40);
        sg.generateApplyModelScheme(models + "text_stacking.mod", amls + mode + "_test.aml", performance + "text_applymodel.per");
        sg.generateReadPerformanceScheme(performance + "text_applymodel.per", models + "text_stacking.mod" );
    }

    private void generateAllSchemes(List<String> categories) {
        String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");

        for (String mode : modes) {
            schemeGen(mode, categories);
        }
    }

    private void executeProcessesInRM(String schemes, List<String> categories) {
        RapidMinerExecutor rapidMinerExecutor =
                new RapidMinerExecutor("C:\\My Programs\\RapidMiner\\RapidMiner5\\scripts\\rapidminer.bat");

        for (int i = 0; i < categories.size(); i++) {
            rapidMinerExecutor.executeProcessInRM(schemes + "1)textBase_" + ( i + 1 ) + ".rmp");
        }
        rapidMinerExecutor.executeProcessInRM(schemes + "2)text_Stacking.rmp");
        rapidMinerExecutor.executeProcessInRM(schemes + "3)text_applyModel.rmp");
    }

    private void executeAll(List<String> categories) {
        String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");

        for (String mode : modes) {
            executeProcessesInRM(experiments + "\\schemes\\" + mode + "\\", categories);
        }
    }

    private void readPerformance(String mode) {
        String performance = experiments + "/performance/" + mode + "/" + mode + "_applymodel.per";
        String out = experiments + "/reports/" + mode + "/" + mode + "_report.xlsx";
        MiscUtils.createParentDirectories(out);
        PerformanceReader performanceReader = new PerformanceReader(performance, out);
        performanceReader.readPerformance();
    }

    private void readAllPerformance() {
        String[] modes = MiscUtils.readPropertiesAsArray(configName, "mode");
        for (String mode : modes) {
            readPerformance(mode);
        }
    }

    private void extractNGrams(List<String> categories) {
        NGramExtractor nGramExtractor = new NGramExtractor(dbUtils);
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);
        nGramExtractor.extractNGrams(IDsByCategory);
    }

    private void dbToFile() {
        dbUtils.dbToFile("select * from list_of_categories", dbUtils, "out.csv");
    }

    private void newExperiment(String expName, List<String> categories) {
        addExperiment(expName);
        int expNumber = getExperimentNumber(expName);

        prepareAMLContents(expName, expNumber, categories);
        writeAMLDATForOneExperiment(expName, expNumber, categories);
        schemeGen(expName, categories);
        executeProcessesInRM(experiments + "\\schemes\\" + expName + "\\", categories);
        readPerformance(expName);
        writeAMLDATForAll(categories);
    }

    private void extractTags(List<String> categories) {
        TagExtractor tagExtractor = new TagExtractor(dbUtils);
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);
        tagExtractor.extractTags(IDsByCategory);
    }

    private void createTagDictionary(List<String> categories){
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);
        TagDictionary tagDictionary = new TagDictionary(dbUtils, 2);
        tagDictionary.createDictionary(IDsByCategory);
    }

    private void prepareTagAML(String expName) {
        addExperiment(expName);
        int expNumber = getExperimentNumber(expName);
        TagAML tagAML = new TagAML(dbUtils);
        tagAML.prepareTagAML(50, expNumber);
    }

    private int getExperimentNumber(String expName) {
        String selectQuery = "select exp_id from experiments where exp_name = ?";
        List<Object[]> res = dbUtils.select(selectQuery, expName);
        // res should be not empty
        if (res.size() == 0) System.exit(-1);

        int expNumber = (int) res.get(0)[0];
        // decrement experiment number by 1, because db numeration starts from 1
        expNumber -= 1;
        return expNumber;
    }

    private void prepareTagDAT(List<String> categories, String expName) {
        TagDAT tagDAT = new TagDAT(dbUtils, 2);
        Map<String, List<Integer>> IDsByCategory = retrieveIDsByCategory(categories);
        int expNum = getExperimentNumber(expName);

        List<Object[]> res = dbUtils.select("select feature_val\n" +
                "from amls\n" +
                "where experiment_num = ?", expNum);
        List <String> tagDictionary = res.stream()
                .map(entry -> entry[0].toString())
                .collect(Collectors.toCollection(LinkedList::new));

        tagDAT.prepareDAT(IDsByCategory, expNum, tagDictionary);
    }

    public static void main(String[] args) {
        Launcher launcher = new Launcher();
        List <String> categories = launcher.chooseCategories();
//        launcher.downloadBlacklists();
//        launcher.uncompressBlacklists();
//        launcher.importBlacklists();
//        launcher.downloadHTML(categories);
//        launcher.extractMainText(categories);
//        launcher.extractTextFromTags(categories);
//        launcher.initialSettings(categories);
//        launcher.prepareAllAMLContents(categories);
//        launcher.writeAMLDATForAll(categories);
//        launcher.writeAMLDATForeachExperiment(categories);
//        launcher.writeAMLDATForAll(categories);
//        launcher.generateAllSchemes(categories);
//        launcher.executeAll(categories);
//        launcher.readAllPerformance();

//        launcher.extractNGrams(categories);
//        launcher.addExperiment("ngrams");
//        launcher.prepareAMLContents("ngrams", 11, categories);
//        launcher.writeAMLDATForOneExperiment("ngrams", 11, categories);
//        launcher.schemeGen("ngrams", categories);
//        launcher.executeProcessesInRM(launcher.experiments + "\\schemes\\ngrams\\", categories);
//        launcher.readPerformance("ngrams");
//        launcher.writeAMLDATForAll(categories);
//        launcher.dbToFile();

//        launcher.extractTags(categories);
//        launcher.createTagDictionary(categories);
//        launcher.prepareTagAML("tag");
//        launcher.prepareTagDAT(categories, "tag");
        launcher.writeAMLDATForOneExperiment("tag", 12, categories);

    }
}
