package aml_dat.createDictionary;

import dbUtils.DbUtils;

import java.io.*;
import java.util.*;


public class CreateDictionary {
    private Vector<String> stopWords;
    private Map<String, Map<String, Float>> tfMap;
    private Map<String, Integer> dcountMap;
    private Map<String, Float> idfMap;
    private Vector<String> dictionary;
    private int categoryCount = 0;
    private DbUtils dbUtils;

    public CreateDictionary(DbUtils dbUtils, String stopWordsPath) throws Exception {
        tfMap = new LinkedHashMap<>();
        dcountMap = new LinkedHashMap<>();
        idfMap = new LinkedHashMap<>();
        stopWords = getStopWords(stopWordsPath);
        dictionary = new Vector<>();
        this.dbUtils = dbUtils;
    }

    private Vector<String> getStopWords(String path) {
        Vector<String> stopwords = new Vector<>();

        try {
            FileInputStream fstream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            //Read File Line By Line
            String strLine;
            while ((strLine = br.readLine()) != null) {
                strLine = strLine.replace(System.getProperty("line.separator"), "");
                if (!strLine.equals("")) {
                    stopwords.add(strLine);
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stopwords;
    }

    void setCategoryCount(int categoryCount) {
        this.categoryCount = categoryCount;
    }

    private String stemmString(String str) throws Exception {
        Stemmer s = new Stemmer();

        String res = "";
        char[] w = new char[501];
        InputStream in = new ByteArrayInputStream(str.toLowerCase().getBytes());
        while (true) {
            int ch = in.read();
            if (Character.isLetter((char) ch)) {
                int j = 0;
                while (true) {
                    ch = Character.toLowerCase((char) ch);
                    w[j] = (char) ch;
                    if (j < 500)
                        j++;
                    ch = in.read();
                    if (!Character.isLetter((char) ch)) {
            /* to test add(char ch) */
                        for (int c = 0; c < j; c++)
                            s.add(w[c]);

            /* or, to test add(char[] w, int j) */
            /* s.add(w, j); */

                        s.stem();
                        {
                            String u;

              /* and now, to test toString() : */
                            u = s.toString();

              /* to test getResultBuffer(), getResultLength() : */
              /* u = new String(s.getResultBuffer(), 0, s.getResultLength()); */

                            res += u;
                        }
                        break;
                    }
                }
            }
            if (ch < 0)
                break;
            res += (char) ch;
        }
        return res;
    }

    void parseCategory(List <String> textsInCategory, String categoryName,
                       boolean isTFCorrect) throws Exception {
        // textsInCategory - text contents from db, belonging to the same category categoryName
        categoryName = categoryName.replaceAll("[^A-Za-z0-9]", "");
        Map<String, Integer> termCount = new LinkedHashMap<>();
        Map<String, Float> tflocalMap = new LinkedHashMap<>();
        Integer wordsCount = 0;

        for (String text : textsInCategory) {
            Vector<String> vec = new Vector<>();
            String fileText = text;

            fileText = fileText.replaceAll("[^A-Za-z ]+", " ");
            while (fileText.contains("  "))
                fileText = fileText.replaceAll("  +", " ");

            fileText = stemmString(fileText);

            for (String term : fileText.split(" ")) {
                if (term.length() > 2 && !stopWords.contains(term) &&
                        (!vec.contains(term) || isTFCorrect)) {
                    wordsCount++;
                    vec.add(term);
                    if (termCount.containsKey(term)) {
                        termCount.put(term, termCount.get(term) + 1);
                    } else {
                        termCount.put(term, 1);
                    }
                }
            }
//            System.out.println("  done. termCount.size=" + termCount.size());
        }

        Map<Float, Vector<String>> tmpToSortMap = new HashMap<>();

        for (String term : termCount.keySet()) {
            Float value = (float) termCount.get(term) / (float) wordsCount;
            if (tmpToSortMap.containsKey(value)) {
                tmpToSortMap.get(value).add(term);
            } else {
                Vector<String> vec = new Vector<>();
                vec.add(term);
                tmpToSortMap.put(value, vec);
            }
            if (dcountMap.containsKey(term)) {
                dcountMap.put(term, dcountMap.get(term) + 1);
            } else {
                dcountMap.put(term, 1);
            }
        }
        while (tmpToSortMap.size() > 0) {
            Float maxValue = tmpToSortMap.keySet().iterator().next();
            for (Float value : tmpToSortMap.keySet()) {
                if (value > maxValue)
                    maxValue = value;
            }
            Vector<String> vec = tmpToSortMap.get(maxValue);
            for (String term : vec) {
                tflocalMap.put(term, maxValue);
            }
            tmpToSortMap.remove(maxValue);
        }
        tfMap.put(categoryName, tflocalMap);

        for (String term : tflocalMap.keySet()) {
            dbUtils.query( "insert into local_tf (category_name, term, value) values (?, ?, ?) ",
                    categoryName, term.replace(".", ","),
                    tflocalMap.get(term).toString().replace(".", ",") );
        }
        System.out.println("Done with: " + categoryName);
    }

    void createIDFList(boolean isIDFcorrect, float treshold) throws Exception {
        Map<Float, Vector<String>> tmpToSortMap = new HashMap<>();

        for (String term : dcountMap.keySet()) {

            Float value;
            if (isIDFcorrect) {
                value = (float) Math.log(categoryCount / (float) dcountMap.get(term));
            } else {
                Float tfsum = (float) 0.0;
                int test = 0;
                for (Iterator iter = tfMap.keySet().iterator(); iter.hasNext(); ) {
                    Map map = tfMap.get(iter.next());
                    if (map.containsKey(term)) {
                        Float tf = (Float) map.get(term);
                        if (tf > treshold)
                            test++;
                        tfsum += tf;
                    }
                }
                if (test != 0)
                    value = (float) Math.log(categoryCount / test);
                else
                    value = (float) 0;
            }

            if (tmpToSortMap.containsKey(value)) {
                tmpToSortMap.get(value).add(term);
            } else {
                Vector<String> vec = new Vector<>();
                vec.add(term);
                tmpToSortMap.put(value, vec);
            }
        }
        while (tmpToSortMap.size() > 0) {
            Float maxValue = tmpToSortMap.keySet().iterator().next();
            for (Float value : tmpToSortMap.keySet()) {
                if (value > maxValue)
                    maxValue = value;
            }
            Vector<String> vec = tmpToSortMap.get(maxValue);
            for (String term : vec) {
                idfMap.put(term, maxValue);
            }
            tmpToSortMap.remove(maxValue);
        }

        for (String term : idfMap.keySet()) {
            dbUtils.query("insert into idf (term, value) values (?, ?)",
                    term.replace(".", ","), idfMap.get(term).toString().replace(".", ","));
        }
    }

    void createTFIDFList(String categoryName, int categoryNum) throws Exception {
        categoryName = categoryName.replaceAll("[^A-Za-z0-9]", "");
        Map<String, Float> tfidfMap = new LinkedHashMap<>();
        Map<String, Float> tflocalMap = tfMap.get(categoryName);

        Map<Float, Vector<String>> tmpToSortMap = new HashMap<>();
        for (String term : tflocalMap.keySet()) {
            Float value = tflocalMap.get(term) * idfMap.get(term);
            if (tmpToSortMap.containsKey(value)) {
                tmpToSortMap.get(value).add(term);
            } else {
                Vector<String> vec = new Vector<>();
                vec.add(term);
                tmpToSortMap.put(value, vec);
            }
        }
        while (tmpToSortMap.size() > 0) {
            Float maxValue = tmpToSortMap.keySet().iterator().next();
            for (Float value : tmpToSortMap.keySet()) {
                if (value > maxValue)
                    maxValue = value;
            }
            Vector<String> vec = tmpToSortMap.get(maxValue);
            for (String term : vec) {
                tfidfMap.put(term, maxValue);
            }
            tmpToSortMap.remove(maxValue);
        }

        int count = 0;
        for (String term : tfidfMap.keySet()) {
            count++;
            if (count <= 1000) {
                String leadingZero;
                if (categoryNum < 10) {
                    leadingZero = "0";
                } else {
                    leadingZero = "";
                }
                dictionary.add(leadingZero + categoryNum + " - " + categoryName + "_" + count + "_" + term);
            }

            dbUtils.query("insert into local_tfidf (category_name, term, value) values (?, ?, ?) ",
                    categoryName, term.replace(".", ","), tfidfMap.get(term).toString().replace(".", ",") );
        }
    }

    void saveDictionary() throws Exception {
        for (String term : dictionary) {
            dbUtils.query("insert into dictionary (term) values (?) ", term);
        }
    }
}