package aml_dat.createDictionary;

import dbUtils.DbUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CreateDictionaryMain {

    private DbUtils dbUtils;
    private String configFilePath;
    private String stopWordsPath;

    public CreateDictionaryMain(DbUtils dbUtils, String configFilePath, String stopWordsPath) {
        this.dbUtils = dbUtils;
        this.configFilePath = configFilePath;
        this.stopWordsPath = stopWordsPath;
    }

    private void dropTables () {
        if (dbUtils.tableExists("idf")) {
            dbUtils.query("drop table idf");
        }
        if (dbUtils.tableExists("local_tf")) {
            dbUtils.query("drop table local_tf");
        }
        if (dbUtils.tableExists("local_tfidf")) {
            dbUtils.query("drop table local_tfidf");
        }
        if (dbUtils.tableExists("dictionary")) {
            dbUtils.query("drop table dictionary");
        }
    }

    private void createTables() {
        if (!dbUtils.tableExists("local_tf")) {
            dbUtils.query("create table local_tf " +
                    " ( id serial not null primary key, category_name varchar(300), " +
                    "term text, value varchar(50) ) ");
            System.out.println("local_tf table created");
        } else {
            System.out.println("local_tf table exists");
        }


        if (!dbUtils.tableExists("local_tfidf")) {
            dbUtils.query("create table local_tfidf " +
                    " ( id serial not null primary key, category_name varchar(300), " +
                    "term text, value varchar(50) ) ");
            System.out.println("local_tfidf table created");
        } else {
            System.out.println("local_tfidf table exists");
        }


        if (!dbUtils.tableExists("idf")) {
            dbUtils.query("create table idf " +
                    " ( id serial not null primary key, " +
                    "term text, value varchar(50) ) ");
            System.out.println("idf table created");
        } else {
            System.out.println("idf table exists");
        }


        if (!dbUtils.tableExists("dictionary")) {
            dbUtils.query("create table dictionary " +
                    " ( id serial not null primary key, " +
                    "term text ) ");
            System.out.println("dictionary table created");
        } else {
            System.out.println("dictionary table exists");
        }
    }

    private void checkProperties(Properties prop, String configFilePath) {
        try {
            File propertyFile = new File(configFilePath);
            if (!propertyFile.exists() || !propertyFile.isFile()) {
                System.err.println("Error while intializing: " + "Property file " +
                        propertyFile.getName() + " isn't exist");
                System.exit(-1);
            }
            InputStream is = new FileInputStream(propertyFile);
            prop.load(is);
            is.close();

            if (!prop.containsKey("IDF_Treshold") ||
                    !prop.getProperty("IDF_Treshold").matches("\\d+\\.\\d+")) {
                System.err.println("Error while intializing: " +
                        "Property file " + propertyFile.getName() + " doesn't contains IDF_Treshold field");
                System.exit(-1);
            }
            if (!prop.containsKey("IDF_Type") ||
                    !prop.getProperty("IDF_Type").matches("[SMsm]")) {
                System.err.println("Error while intializing: "
                        + "Property file " + propertyFile.getName() + " doesn't contains IDF_Type field");
                System.exit(-1);
            }

            if (!prop.containsKey("TF_Type") ||
                    !prop.getProperty("TF_Type").matches("[SMsm]")) {
                System.err.println("Error while intializing: " +
                        "Property file " + propertyFile.getName() + " doesn't contains TF_Type field");
                System.exit(-1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createDictionary(Map<String, List<String>> dataSet) {
        try {
            Properties prop = new Properties();
            checkProperties(prop, configFilePath);
            boolean isTFcorrect = prop.getProperty("TF_Type").matches("[Ss]");
            boolean isIDFcorrect = prop.getProperty("IDF_Type").matches("[Ss]");
            String tresholdStr = prop.getProperty("IDF_Treshold");
            float treshold = Float.parseFloat(tresholdStr);

            Set<String> chosenCategories = dataSet.keySet();

            dropTables();
            createTables();

            CreateDictionary createdictionary = new CreateDictionary(dbUtils, stopWordsPath);

            System.out.println("Categories used: ");
            System.out.println(chosenCategories);

            for (String category : chosenCategories) {
                System.out.println("Creating tf list for " + category);
                List<String> textsInCategory = dataSet.get(category);

                System.out.println("Texts in category size: " + textsInCategory.size());
                createdictionary.parseCategory(textsInCategory, category, isTFcorrect);
            }

            int categoryCount = chosenCategories.size();
            createdictionary.setCategoryCount(categoryCount);

            System.out.println("Creating idf list");
            createdictionary.createIDFList(isIDFcorrect, treshold);
            System.out.println("Done.");

            int index = 0;

            for (String cat : chosenCategories) {
                System.out.println("Creating tfidf list for " + cat);
                createdictionary.createTFIDFList(cat, ++index);
                System.out.println("Done.");
            }
            createdictionary.saveDictionary();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
