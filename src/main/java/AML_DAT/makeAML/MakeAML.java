package aml_dat.makeAML;

import dataSplitter.DataSetTools;
import dbUtils.DbUtils;

import java.io.*;
import java.util.*;

public class MakeAML {

    private final DbUtils dbUtils;
    private String mode;
    private String path;
    private int[] count;

    public MakeAML(DbUtils dbUtils, String mode, String path, int[] count) {
        this.dbUtils = dbUtils;
        this.mode = mode;
        this.path = path;
        this.count = count;
    }

    private void createTables (int experimentNum) {

        if (experimentNum == 0) {
            if (dbUtils.tableExists("datasets")) {
                dbUtils.query("drop table datasets");
                System.out.println("Previous datasets table dropped");
            }

            dbUtils.query("CREATE TABLE dataSets (\n" +
                    "  website_id integer NOT NULL,\n" +
                    "  website_name text,\n" +
                    "  features text,\n" +
                    "  fileLength integer,\n" +
                    "  categories_basis text,\n" +
                    "  isUnknown BOOLEAN,\n" +
                    "  experiment_num INTEGER,\n" +
                    "  PRIMARY KEY (website_id, experiment_num)\n" +
                    ")");
            System.out.println("Table for dataSets created");

            if (dbUtils.tableExists("amls")) {
                dbUtils.query("drop table amls");
                System.out.println("AMLs table dropped");
            }

            dbUtils.query("CREATE table AMLs (\n" +
                    "  feature_id SERIAL PRIMARY KEY NOT NULL,\n" +
                    "  feature_val text,\n" +
                    "  experiment_num integer\n" +
                    ")");
        }
    }

    public void makeAML(List<String> chosenCategories, String tableName, String fieldName, int experimentNum) {

        createTables(experimentNum);
        DataSetTools dataSetTools = new DataSetTools(dbUtils);

        try {
            PrepareAMLByDictionary prepareAMLByDictionary =
                    new PrepareAMLByDictionary(path, count, dbUtils, mode, experimentNum);

            FileWriter[] fw1 = new FileWriter[count.length];

            for (int i = 0; i < count.length; i++) {
                fw1[i] = new FileWriter(path + mode + '_' + "texts_" + count[i] + "_bin.dat");
            }

            System.out.println("Categories used: ");
            System.out.println(chosenCategories);

            for (String category : chosenCategories) {
                System.out.println("Processing: " + category + " now");

                // filename = text_data
                Map <String, String> namedTexts =
                        dataSetTools.createValidDataNames(category, tableName, fieldName);

                try {
                    // deal with dataSet (not unknowns)
                    prepareAMLByDictionary.parseDirectory(fw1, namedTexts, category);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Map <String, List<String>> unknownFilenamesByCategory
                    = dataSetTools.createUnknownDataNames(chosenCategories, tableName);

            // add unknowns
            prepareAMLByDictionary.addUnknownSites(unknownFilenamesByCategory,
                    fw1, (chosenCategories.size() * 1000));
            for (int i=0; i< count.length; i++) {
                fw1[i].close();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
