package aml_dat.makeAML;

import dbUtils.DbUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PrepareAMLByDictionary {

    private DbUtils dbUtils;
    private int experimentNum;

    private Vector<String>[] dictionaryWords;
    private Vector<String> categories = new Vector();

    public PrepareAMLByDictionary(String path, int[] count,
                                  DbUtils dbUtils, String mode, int experimentNum) throws Exception {

        this.dbUtils = dbUtils;
        this.experimentNum = experimentNum;

        Vector<String>[] dictionary = new Vector[count.length];
        dictionaryWords = new Vector[count.length];
        for (int i = 0; i < count.length; i++) {
            dictionary[i] = new Vector<>();
            dictionaryWords[i] = new Vector<>();
        }

        // fetching dictionary items
        List<Object[]> select = dbUtils.select("select term " +
                "from dictionary");

        List<String> wordsFromDictionary = select
                .stream()
                .map(text -> (String) text[0])
                .collect(Collectors.toCollection(LinkedList::new));

        for (String word : wordsFromDictionary) {
            Pattern p = Pattern.compile("(\\d+) \\- ([A-Za-z&_ ]+)_(\\d+)_(.[a-z]+)");
            Matcher m = p.matcher(word);
            if (m.find(0)) {
                if (m.group(3).matches("\\d+")) {
                    String categoryMin = m.group(1) + "_"
                            + m.group(2).replaceAll("[^A-Za-z&_]+", "_").replaceAll("__+", "_");
                    if (!categories.contains(categoryMin))
                        categories.add(categoryMin);

                    for (int i = 0; i < count.length; i++) {
                        if (Integer.parseInt(m.group(3)) <= count[i]) {
                            dictionary[i].add(((m.group(1).length() < 3) ? "0" : "")
                                    + ((m.group(1).length() < 2) ? "0" : "")
                                    + m.group(1) + "_" + ((m.group(3).length() < 3) ? "0" : "")
                                    + ((m.group(3).length() < 2) ? "0" : "")
                                    + m.group(3) + "_" + m.group(4));
                            this.dictionaryWords[i].add(m.group(4));
                        }
                    }
                }
            }
        }

        FileWriter[] fw1 = new FileWriter[count.length];

        for (int i = 0; i < count.length; i++) {
            String dir = path;
            path = path + mode + '_' + "texts_" + count[i] + "_bin.aml";
            System.out.println(path);
            File yourFile = new File(dir);

            if(!yourFile.exists()) {
                if (!yourFile.mkdirs()) {
                    System.out.println("file texts_50_bin.aml wasn't created!");
                }
            }
            fw1[i] = new FileWriter(path);

            fw1[i].write("<?xml version=\"1.0\" encoding=\"windows-1251\" standalone=\"no\"?>\n");
            fw1[i].write("<attributeset default_source=\"" + mode + '_' + "texts_"
                    + count[i] + "_" + "bin.dat\" encoding=\"windows-1251\">\n");
            int countParam = 1;

            fw1[i].write("<attribute name=\"" + mode + '_' + "siteID" + "\" sourcecol=\"" + countParam
                    + "\" valuetype=\"integer\"/>\n");
            countParam++;

            for (String word : dictionary[i]) {
                fw1[i].write("<attribute name=\"" + mode + '_' + word + "\" sourcecol=\"" + countParam
                        + "\" valuetype=\"binominal\">\n");

                dbUtils.query("insert into AMLs (feature_val, experiment_num) " +
                        " values (?, ?)", mode + '_' + word, experimentNum);

                fw1[i].write("  <value>0</value>\n");
                fw1[i].write("  <value>1</value>\n");
                fw1[i].write("</attribute>\n");
                countParam++;
            }

            fw1[i].write("<attribute name=\"" + mode + '_' + "fileLength" + "\" sourcecol=\""
                    + countParam + "\" valuetype=\"integer\"/>\n");
            countParam++;

            for (String category : categories) {
                fw1[i].write("<attribute name=\"" + /*mode + '_' + */ "label_" + category.
                        replaceAll("[^A-Z0-9a-z_]+", "_")
                        .replaceAll("__+", "_") + "\" sourcecol=\"" + countParam + "\" valuetype=\"binominal\">\n");
                fw1[i].write("  <value>0</value>\n");
                fw1[i].write("  <value>1</value>\n");
                fw1[i].write("</attribute>\n");
                countParam++;
            }

            fw1[i].write("<attribute name=\"" /*+ mode + '_'*/ + "label\" sourcecol=\"" + countParam + "\" valuetype=\"nominal\">\n");

            for (String category : categories) {
                fw1[i].write("  <value>" +
                        category.substring(category.indexOf("_") + 1).replaceAll("&", "&amp;") + "</value>\n");
            }
            fw1[i].write("  <value>Unknown</value>\n");
            fw1[i].write("</attribute>\n");
            countParam++;
            fw1[i].write("</attributeset>\n");
            fw1[i].close();
        }
    }

    private String stemmString(String str) throws Exception {
        Stemmer s = new Stemmer();

        String res = "";
        char[] w = new char[501];
        InputStream in = new ByteArrayInputStream(str.toLowerCase().getBytes());
        while (true) {
            int ch = in.read();
            if (Character.isLetter((char) ch)) {
                int j = 0;
                while (true) {
                    ch = Character.toLowerCase((char) ch);
                    w[j] = (char) ch;
                    if (j < 500)
                        j++;
                    ch = in.read();
                    if (!Character.isLetter((char) ch)) {
            /* to test add(char ch) */
                        for (int c = 0; c < j; c++)
                            s.add(w[c]);

            /* or, to test add(char[] w, int j) */
            /* s.add(w, j); */

                        s.stem();
                        {
                            String u;

              /* and now, to test toString() : */
                            u = s.toString();

              /* to test getResultBuffer(), getResultLength() : */
              /* u = new String(s.getResultBuffer(), 0, s.getResultLength()); */

                            res += u;
                        }
                        break;
                    }
                }
            }
            if (ch < 0)
                break;
            res += (char) ch;
        }
        return res;
    }

    public void parseDirectory(FileWriter[] fw1, Map<String, String> namedTextsInCategory,
                               String categoryName) throws Exception {
        // map namedText: key = website_url; value = text_contents
        // namedText contains data only belonging to categoryName
        categoryName = categoryName.replaceAll("[^A-Za-z0-9]", "");

        for (Map.Entry entry : namedTextsInCategory.entrySet()) {

            Object[] databaseEntry = new Object[7];

            String website_url = (String) entry.getKey();
            String fileText = (String) entry.getValue();
            long fileLength = fileText.length();

            fileText = fileText.replaceAll("[^A-Za-z ]+", " ");
            fileText = fileText.replaceAll("  +", " ");
            fileText = stemmString(fileText);

            for (int i = 0; i < dictionaryWords.length; i++) {
                fw1[i].write(website_url + "\n");
//                String siteID = website_url.substring(0, website_url.indexOf("_"));
                String siteID = website_url.substring(website_url.indexOf("#") + 1,
                        website_url.indexOf("_"));

                fw1[i].write(siteID + " ");
                databaseEntry[0] = Integer.parseInt(siteID);  // now have website_id
                databaseEntry[1] = website_url;   // now have website_name

                String features = "";
                for (String word : dictionaryWords[i]) {
                    int count = 0;
                    int offset = 0;
                    int foundIndex = -1;
                    while ((foundIndex = (" " + fileText + " ").indexOf(" " + word + " ", offset)) > -1) {
                        count++;
                        offset = foundIndex + 1;
                    }
                    fw1[i].write(((count == 0) ? "\"0\"" : "\"1\"") + " ");
                    features += ((count == 0) ? "\"0\"" : "\"1\"") + " ";
                }
                databaseEntry[2] = features;    // features added

                fw1[i].write(fileLength + " ");
                databaseEntry[3] = fileLength;  // now we have fileLength

//                features += fileLength + " ";
                String categoriesBasis = "";

                for (String category : categories) {
                    String categoryMin = categoryName.replaceAll("[^A-Za-z_]+", "_").replaceAll("__+", "_");
                    fw1[i].write(((category.indexOf(categoryMin) > -1) ? "\"1\"" : "\"0\"") + " ");
//                    features += ((category.indexOf(categoryMin) > -1) ? "\"1\"" : "\"0\"") + " ";
                    categoriesBasis += ((category.indexOf(categoryMin) > -1) ? "\"1\"" : "\"0\"") + " ";
                }
                fw1[i].write(categoryName + "\n");
                categoriesBasis += categoryName;
                databaseEntry[4] = categoriesBasis; // now we have categoriesBasis
//                features += categoryName;

                databaseEntry[5] = false; // isUnknown = false
                databaseEntry[6] = experimentNum;  // add experiment number

                dbUtils.query("insert into dataSets (website_id, website_name, features, " +
                        " fileLength, categories_basis, isUnknown, experiment_num) values (?, ?, ?, ?, ?, ?, ?)",
                        databaseEntry[0], databaseEntry[1], databaseEntry[2], databaseEntry[3],
                        databaseEntry[4], databaseEntry[5], databaseEntry[6]);
            }
        }
        System.out.println("Done with: " + categoryName);
    }

    void addUnknownSites(Map <String, List<String>> unknownFilenamesByCategory,
                         FileWriter[] fw1, int countSites) throws Exception {
        // foreach category
        for (String category : unknownFilenamesByCategory.keySet()) {
            List<String> unknownFilenames = unknownFilenamesByCategory.get(category);
            // foreach filename inside category
            for (String unknownFilename : unknownFilenames) {
                // foreach word in a dictionary basis

                for (int i = 0; i < dictionaryWords.length; i++) {

                    Object[] databaseEntry = new Object[7];

                    fw1[i].write(unknownFilename + "\n");
                    String siteID = unknownFilename.substring(unknownFilename.indexOf("#") + 1,
                            unknownFilename.indexOf("_"));

                    databaseEntry[0] = Integer.parseInt(siteID);    // website_id created
                    databaseEntry[1] = unknownFilename;     // website_name for unknown site created
                    String features = "";

                    fw1[i].write(siteID + " ");
                    for (String word : dictionaryWords[i]) {
                        int count = 0;
                        fw1[i].write(((count == 0) ? "\"0\"" : "\"1\"") + " ");
                        features += ((count == 0) ? "\"0\"" : "\"1\"") + " ";
                    }

                    databaseEntry[2] = features;        // features created

                    int intSiteID = Integer.parseInt(siteID);
                    String length = dbUtils.select("SELECT length(text_contents)\n" +
                                    "from text_data\n" +
                                    "where website_id = ? ", intSiteID).get(0)[0].toString();

                    databaseEntry[3] = Integer.parseInt(length);      // fileLength created

                    fw1[i].write(length + " ");  // here should be fileLength!!!!!!!!!
//                    features += length + " ";

                    String categoriesBasis = "";
                    for (String categoryName : categories) {
//                        fw1[i].write("\"0\" ");

                        if (categoryName.contains(category)) {
                            fw1[i].write("\"1\" ");
                            categoriesBasis += "\"1\" ";
//                            features += "\"1\" ";
                        } else {
                            fw1[i].write("\"0\" ");
//                            features += "\"0\" ";
                            categoriesBasis  += "\"0\" ";
                        }
                    }
//                    features += "Unknown";
                    categoriesBasis += category;
                    databaseEntry[4] = categoriesBasis;     // categoriesBasis created
                    databaseEntry[5] = true;                // isUnknown = true
                    databaseEntry[6] = experimentNum;       // experimentNumber created
                    dbUtils.query("insert into dataSets (website_id, website_name, features, " +
                                    " fileLength, categories_basis, isUnknown, experiment_num) values (?, ?, ?, ?, ?, ?, ?)",
                            databaseEntry[0], databaseEntry[1], databaseEntry[2], databaseEntry[3],
                            databaseEntry[4], databaseEntry[5], databaseEntry[6]);

                    fw1[i].write("Unknown\n");
                }
            }
        }

        for (int j = 0; j < countSites; j++) {
            for (int i = 0; i < dictionaryWords.length; i++) {
                fw1[i].write("#Unknown_" + (Integer.MAX_VALUE - 100000 + j) + "\n");
                int siteID = Integer.MAX_VALUE - 100000 + j;        // check if correct !!!
                fw1[i].write(siteID + " ");
                for (String word : dictionaryWords[i]) {
                    int count = 0;
                    fw1[i].write(((count == 0) ? "\"0\"" : "\"1\"") + " ");
                }
                fw1[i].write(0 + " ");

                for (String category : categories) {
                    fw1[i].write("\"0\" ");
                }
                fw1[i].write("Unknown\n");
            }
        }
    }
}
