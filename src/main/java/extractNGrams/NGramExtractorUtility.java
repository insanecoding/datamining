package extractNGrams;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class NGramExtractorUtility {
    private List<String> generateNGram(List<String> collected, String word, int nGramSize) {
        for (int i = 0; i < word.length(); i++) {
            if ( (i + nGramSize - 1) < word.length() ) {
                String nGram = word.substring(i, i + nGramSize);
                // add to list if the string is not only digits
                if (!StringUtils.isNumeric(nGram)) {
                    collected.add(nGram);
                }
            }
        }
        return collected;
    }

    public List<String> generateNGrams (String input, int minNGramSize, int maxNGramSize) {
        List <String> collectedNGrams = new LinkedList<>();
        for (int curNGramSize = minNGramSize; curNGramSize <= maxNGramSize; curNGramSize++) {
            collectedNGrams = generateNGram(collectedNGrams, input, curNGramSize);
        }
        return collectedNGrams;
    }

    public List<String> generateNGrams (String input, int minNGramSize) {
        List <String> collectedNGrams = new LinkedList<>();
        int maxNGramSize = input.length();
        for (int curNGramSize = minNGramSize; curNGramSize <= maxNGramSize; curNGramSize++) {
            collectedNGrams = generateNGram(collectedNGrams, input, curNGramSize);
        }
        return collectedNGrams;
    }

    public List<String> generateNGrams (String input) {
        List <String> collectedNGrams = new LinkedList<>();
        int minNGramSize = 2;
        int maxNGramSize = input.length();
        for (int curNGramSize = minNGramSize; curNGramSize <= maxNGramSize; curNGramSize++) {
            collectedNGrams = generateNGram(collectedNGrams, input, curNGramSize);
        }
        return collectedNGrams;
    }
}
