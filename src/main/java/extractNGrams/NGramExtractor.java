package extractNGrams;

import dbUtils.DbUtils;

import java.util.List;
import java.util.Map;

public class NGramExtractor {

    private DbUtils dbUtils;

    public NGramExtractor(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    private void checkTables() {
        if (!dbUtils.tableExists("list_of_websites")) {
            System.exit(-1);
        }
        // drop output table if it exists
        if (dbUtils.tableExists("ngrams")) {
            dbUtils.query("drop table ngrams");
            System.out.println("Ngrams table dropped");
        }

        dbUtils.query("create table ngrams (\n" +
                    "  website_id INTEGER PRIMARY KEY NOT NULL REFERENCES list_of_websites (id),\n" +
                    "  ngram text\n" +
                    ")");
        System.out.println("Ngrams table created");
    }

    private String processURL(String url) {
        String out;
        NGramExtractorUtility nGramExtractorUtility = new NGramExtractorUtility();
        // remove "http://" prefix
        String[] tokens = url.split("//");
        // lets find website extension (.com / .de / .eu etc)
        int trailingPointIdx = tokens[1].lastIndexOf(".");
        // remove website extension, leave only website body
        String urlBody = tokens[1].substring(0, trailingPointIdx);
        // applying regexp to clean data: leave only letters and digits
        urlBody = urlBody.replaceAll("[^A-Za-z0-9]", "");
        // extract NGrams (all possible)
        List<String> grams = nGramExtractorUtility.generateNGrams(urlBody);
        String res =  grams.toString();
        out = res.substring(1, res.length() - 1);
        out = out.replaceAll(",", "");
        return out;
    }

    public void extractNGrams(Map<String, List<Integer>> IDsByCategory) {
        checkTables();

        IDsByCategory.forEach((category, ids) -> ids.forEach(id -> {
            List<Object[]> res = dbUtils.select("select website_url\n" +
                    "from list_of_websites\n" +
                    "where id = ?", id);

            if (res.size() != 0) {
                String url = (String) res.get(0)[0];
                String output = processURL(url);

                if (!output.equals("")) {
                    dbUtils.query("insert into ngrams (website_id, ngram) values (?, ?) ", id, output);
                }
            }
        }));
    }
}
