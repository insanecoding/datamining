package dbUtils;

import utils.MiscUtils;

public class DbUtilsFactory {
    /**
     * creates <code>DbUtils</code> instance
     * @param propertyFile config file, containing user, password, db, driver, port entries
     * @return new DbUtils object
     */
    public static DbUtils createDbUtils(String propertyFile) {
        String user = MiscUtils.readProperty(propertyFile, "user");
        String pass = MiscUtils.readProperty(propertyFile, "password");
        String host = MiscUtils.readProperty(propertyFile, "host");
        String db = MiscUtils.readProperty(propertyFile, "db");
        String driver = MiscUtils.readProperty(propertyFile, "driver");
        return new DbUtils(host, db, driver, user, pass);
    }
}
