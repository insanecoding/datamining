/**
 * This package contains database operations: query execution, <br>
 *     checking of database/table/entry existence
 */
package dbUtils;