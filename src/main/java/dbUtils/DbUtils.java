package dbUtils;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ArrayListHandler;

public class DbUtils {

    private String driver;
    private String user;
    private String password;
    private String host;
    private String url;

    private DataSource ds;
    private QueryRunner qr;
    private ResultSetHandler<List<Object[]>> rsh = new ArrayListHandler();

    /**
     * Initializes <code>DbUtils</code> class
     * with specified parameters (host, db, driver, user, password)
     * For example, if specified database doesn't exist, it will be created
     * @param host host-name
     * @param db name of the database to associate DbUtils with
     *           (will be created if not exists)
     * @param driver driver name
     * @param user user name
     * @param password password to database
     */
    public DbUtils(String host, String db,
                   String driver, String user, String password) {

        this.driver = driver;
        this.user = user;
        this.password = password;
        this.host = host;
        this.url = host + db;

        if (!dbExists(db)) {
            Connection c = null;
            try {
                c = DriverManager.getConnection(host, user, password);
                Statement stmt = c.createStatement();
                int result = stmt.executeUpdate("create database " + '"' + db + '"');
                System.out.println("" + result);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection(c);
            System.out.println("Data source has been created");
        }

        ds = createDataSource();
        qr = new QueryRunner(ds);
    }

    private DataSource createDataSource() {
        BasicDataSource d = new BasicDataSource();
        d.setDriverClassName(driver);
        d.setUsername(user);
        d.setPassword(password);
        d.setUrl(url);
        return d;
    }

    public DataSource getDs() {
        return ds;
    }

    private void closeConnection(Connection c) {
        try {
            if (c != null) c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean dbExists(String database) {      //database name is case sensitive !!!!!
        boolean result = false;
        Connection c = null;
        String url = host + database;

        try {
            c = DriverManager.getConnection(url, user, password);
            DatabaseMetaData md = c.getMetaData();
            ResultSet rs = md.getCatalogs();
            while (rs.next()) {
                if (rs.getString(1).equals(database)) {
                    result = true;
                }
            }
        } catch (SQLException e) {
            result = false;
        }
        closeConnection(c);
        return result;
    }

    public boolean tableExists(String tableName) {
        boolean result = false;
        Connection c = null;
        try {
            c = ds.getConnection();
            ResultSet rs = c.getMetaData()
                    .getTables(null, null, tableName, null);
            while (rs.next()) {
                if (rs.getString(3).equals(tableName)) {
                    result = true;
                }
            }
        } catch (SQLException e) {
            result = false;
        } finally {
            closeConnection(c);
        }
        return result;
    }

    public boolean checkColumn(String inTable, String col) {
        // column name is case sensitive!
        boolean result = false;
        Connection c = null;
        try {
            c = ds.getConnection();
            DatabaseMetaData md = c.getMetaData();
            ResultSet rs = md.getColumns(null, null, inTable, col);
            if (rs.next()) {
                //Column in table exist
                result = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(c);
        }
        return result;
    }

    public boolean checkColumns(String inTable, String... columns) {
        boolean result;
        if (columns.length == 1) {
            result = checkColumn(inTable, columns[0]);
        } else {
            result = true;
            for (String column :
                    columns) {
                result &= checkColumn(inTable, column);
            }
        }
        return result;
    }

    public boolean entryExists(String inTable, String key, String value) {
        boolean res = false;
        String check = "select count(1) from " + inTable + " where " + key + " = '" + value + "'";
        List<Object[]> resultList = select(check);
        long var = (long) resultList.get(0)[0]; // the result of a query is 0 or 1
        if (var == 1) {
            res = true;
        }
        return res;
    }

    public void printResultSet(List<Object[]> resultList) {
        for (Object[] ob : resultList) {
            for (Object obj : ob) {
                System.out.print(obj);
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    public int query(String sql, Object... args) {
        // insert, update or delete
        int result = 0;
        try {
            result = qr.update(sql, args);
        } catch (SQLException e) {
            String message = e.getMessage();
            if (message.contains("ОШИБКА: повторяющееся значение ключа нарушает ограничение уникальности")) {
//                System.out.println("duplicate skipped");
            } else if (message.contains("ОШИБКА: неверная последовательность байт для кодировки \"UTF8\"")) {
                System.out.println("UTF-8 insertion error");
            } else {
                e.printStackTrace();
            }
        }
        return result;
    }

    public List<Object[]> select(String sql, Object... args) {
        List<Object[]> resultList = null;
        try {
            resultList = qr.query(sql, rsh, args);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    public void selectLargeData(Connection conn, String sql) {
        try {
            // make sure autocommit is off (postgres)
            conn.setAutoCommit(false);

            Statement stmt = conn.createStatement();
            stmt.setFetchSize(100);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                System.out.print(rs.getObject("website_id"));
                System.out.print(' ');
                String str = (String) rs.getObject("html_contents");
                System.out.println(str.length());
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dbToFile(String sql, DbUtils dbUtils, String filePath) {
        try (PrintWriter writer = new PrintWriter(filePath)) {
            List<Object[]> res = dbUtils.select(sql);
            if (res.size() != 0) {
                // foreach entry in result set
                for (Object[] entry : res) {
                    String result = "";
                    // for each cell in current entry
                    for (Object entryCell : entry) {
                        result += entryCell.toString() + ";";
                    }
                    // removing ; near the last character
                    result = result.substring(0, result.length() - 1);
                    // writing into file
                    writer.println(result);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
