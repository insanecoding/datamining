package aml_dat.writeAML;

import dbUtils.DbUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class DATWriter {

    private final DbUtils dbUtils;
    private PrintWriter writer;
    private String mode;
    private String outputRoot;
    private int featuresPerCategory;

    /**
     * @param dbUtils             database interaction class
     * @param mode                experiment type: text, tag or all
     * @param outputRoot          where dat files are stored
     * @param featuresPerCategory how much features is there per category
     * @throws IOException exception with files
     */
    public DATWriter(DbUtils dbUtils, String mode, String outputRoot,
                     int featuresPerCategory) throws IOException {
        this.dbUtils = dbUtils;
        this.mode = mode;
        this.outputRoot = outputRoot;
        this.featuresPerCategory = featuresPerCategory;
    }

    private void createSubFolders(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists() && file.getParentFile().mkdirs() && file.createNewFile()) {
            System.out.println("Output file: " + filePath + " created");
        }
    }

    private void closeWriter() {
        writer.close();
    }

    private void writeExperimentData(int experimentNum, Map<String, List<Integer>> dataSet) {
        // for each category
        for (String categoryName : dataSet.keySet()) {
            List<Integer> websiteIDs = dataSet.get(categoryName);
            // for each websiteID in category
            websiteIDs.forEach(websiteID -> {
                List<Object[]> entry = dbUtils.select("select website_id, website_name, " +
                        " features, fileLength, categories_basis\n" +
                        " from datasets\n" +
                        " where experiment_num = ? and website_id = ? ", experimentNum, websiteID);

                if (entry.size() != 0) {
                    String websiteName = (String) entry.get(0)[1];
                    int id = (int) entry.get(0)[0];
                    String features = (String) entry.get(0)[2];
                    int fileLength = (int) entry.get(0)[3];
                    String categoriesBasis = (String) entry.get(0)[4];

                    writer.println(websiteName);
                    writer.println("" + id + ' ' + features + fileLength + ' ' + categoriesBasis);
                }
            });
        }
    }

    private void addUnknowns(int unknownsNumber, Map<String, List<Integer>> learnDataSet) {
        // now add 9000 unknowns
        // firstly, prepare entry for unknowns
        for (int counter = 0; counter < unknownsNumber; counter++) {
            String entryForUnknowns = "";
            int siteID = Integer.MAX_VALUE - 100_000 + counter;
            entryForUnknowns += ("#Unknown_" + siteID + "\n");
            entryForUnknowns += siteID + " ";

            // define features per category
            // if we have 9 categories ==> will be 9*50 = 450 features
            final int categoriesN = learnDataSet.keySet().size();

            if (mode.contains("tag")) {
                for (int i = 0; i < featuresPerCategory; i++) {
                    entryForUnknowns += "\"0.0\"" + " ";
                }
            } else {
                for (int i = 0; i < (categoriesN * featuresPerCategory); i++) {
                    entryForUnknowns += "\"0\"" + " ";
                }
            }

            entryForUnknowns += "0 "; // zero length
            for (int k = 0; k < categoriesN; k++) {
                entryForUnknowns += "\"0\" ";
            }

            entryForUnknowns += "Unknown\n";
            writer.print(entryForUnknowns);
        }
    }

    public void createDATForExperiment(int experimentNum, int unknownsNumber, Map<String, List<Integer>> learnDataSet,
                                       Map<String, List<Integer>> testDataSet) throws IOException {

        // write learning .dat
        createSubFolders(outputRoot + mode + "_learn.dat");
        writer = new PrintWriter(outputRoot + mode + "_learn.dat");
        writeExperimentData(experimentNum, learnDataSet);
        addUnknowns(unknownsNumber, learnDataSet);
        System.out.println("Done for learning dataSet");
        System.out.println("==========");
        closeWriter();

        // write testing .dat
        createSubFolders(outputRoot + "/" + mode + "_test.dat");
        writer = new PrintWriter(outputRoot + "/" + mode + "_test.dat");

        writeExperimentData(experimentNum, testDataSet);
        System.out.println("Done for testing dataSet");
        closeWriter();
    }

//    private void writeAllData(int experimentsN, Map<String, List<Integer>> dataSet) {
//        // foreach category
//        for (String categoryName : dataSet.keySet()) {
//            System.out.println("Working on category: " + categoryName);
//            // getting list of website_ids in it
//            List<Integer> website_ids = dataSet.get(categoryName);
//            // foreach website from the list
//            for (int website_id : website_ids) {
//
//                int textLength = -1;
//                // foreach experiment for this site
//                for (int i = 0; i <= experimentsN; i++) {
//                    List<Object[]> entry = dbUtils.select("select website_id, website_name, " +
//                            " features, fileLength, categories_basis\n" +
//                            " from datasets\n" +
//                            " where experiment_num = ? and website_id = ? ", i, website_id);
//
//                    // writing website name only once, e.g: #2680675_httpchevaldesandescom
//                    // then write website_id
//                    if (i == 0) {   // if experiment is for text_main
//                        int id = (int) entry.get(0)[0];
//                        String websiteName = (String) entry.get(0)[1];
//
//                        List<Object[]> res = dbUtils.select("select fileLength from datasets\n" +
//                                " where experiment_num = 0 and website_id = ? ", website_id);
//                        textLength = (int) res.get(0)[0]; // save textLength only for main text
//
//                        writer.println(websiteName);
//                        writer.print(id + " ");
//                    }
//
//                    // writing features during each iteration
//                    String features = (String) entry.get(0)[2];
//                    writer.print(features);
//
//                    // during the final iteration write textLength and categories basis
//                    if (i == experimentsN) {
//                        String categoriesBasis = (String) entry.get(0)[4];
//                        // check up this!!!
//                        writer.print("" + textLength + " " + categoriesBasis + '\n');
//                    }
//                }
//            }
//        }
//    }

    private void addUnknownsToAll(int experimentsN, int unknownsNumber, int categoriesN) {
        // now add 9000 unknowns
        // firstly, prepare entry for unknowns

        for (int counter = 0; counter < unknownsNumber; counter++) {
            String entryForUnknowns = "";
            int siteID = Integer.MAX_VALUE - 100_000 + counter;
            entryForUnknowns += ("#Unknown_" + siteID + "\n");
            entryForUnknowns += siteID + " ";

            for (int i = 0; i < experimentsN; i++) {
                // if we have 9 categories ==> will be 9*50 = 450 features
                for (int k = 0; k < (categoriesN * featuresPerCategory); k++) {
                    entryForUnknowns += "\"0\"" + " ";
                }
            }

            entryForUnknowns += "0 "; // zero length
            for (int k = 0; k < categoriesN; k++) {
                entryForUnknowns += "\"0\" ";
            }

            entryForUnknowns += "Unknown\n";
            writer.print(entryForUnknowns);
        }
    }

    private void foo(int experimentsN, Map<String, List<Integer>> dataSet) {
        // foreach category
        for (String categoryName : dataSet.keySet()) {
            System.out.println("Working on category: " + categoryName);
            // getting list of website_ids in it
            List<Integer> website_ids = dataSet.get(categoryName);
            // foreach website from the list
            for (int website_id : website_ids) {

                int textLength = -1;
                // foreach experiment for this site
                for (int i = 0; i < experimentsN; i++) {
                    List<Object[]> entry = dbUtils.select("select website_id, " +
                            " website_name, " +
                            " features, fileLength, categories_basis\n" +
                            " from datasets\n" +
                            " where experiment_num = ? and website_id = ? ", i, website_id);

                    if (entry.size() == 0) System.exit(-1);

                    // writing website name only once, e.g: #2680675_httpchevaldesandescom
                    // then write website_id
                    if (i == 0) {   // if experiment is for text_main
                        int id = (int) entry.get(0)[0];
                        String websiteName = (String) entry.get(0)[1];
                        textLength = (int) entry.get(0)[3]; // save textLength only for main text

                        writer.println(websiteName);
                        writer.print(id + " ");
                    }

                    // writing features during each iteration
                    String features = (String) entry.get(0)[2];
                    writer.print(features);

                    // during the final iteration write textLength and categories basis
                    if (i == (experimentsN - 1)) {
                        String categoriesBasis = (String) entry.get(0)[4];
                        // check up this!!!
                        writer.print("" + textLength + " " + categoriesBasis + '\n');
                    }
                }
            }
        }
    }

    public void createDATForAllExperiments(int experimentsN, int unknownsNumber,
                                           Map<String, List<Integer>> learnDataSet,
                                           Map<String, List<Integer>> testDataSet) throws IOException {

        createSubFolders(outputRoot + mode + "_learn.dat");
        writer = new PrintWriter(outputRoot + mode + "_learn.dat");
        foo(experimentsN, learnDataSet);
        addUnknownsToAll(experimentsN, unknownsNumber, learnDataSet.keySet().size());

        System.out.println("Done for learning dataSet");
        closeWriter();

        createSubFolders(outputRoot + "/" + mode + "_test.dat");
        writer = new PrintWriter(outputRoot + "/" + mode + "_test.dat");
        foo(experimentsN, testDataSet);

        System.out.println("Done for testing dataSet");
        closeWriter();
        System.out.println("==========");
    }
}