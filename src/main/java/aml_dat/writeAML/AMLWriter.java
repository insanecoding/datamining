package aml_dat.writeAML;

import dbUtils.DbUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class AMLWriter {

    private final DbUtils dbUtils;
    private PrintWriter writer;

    public AMLWriter(DbUtils dbUtils, String amlFileName) throws IOException {
        this.dbUtils = dbUtils;

        createSubFolders(amlFileName);
        this.writer = new PrintWriter(amlFileName);
    }

    private void createSubFolders(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists() && file.getParentFile().mkdirs() && file.createNewFile()) {
            System.out.println("Output file: " + filePath + " created");
        }
    }

    private void closeWriter() {
        writer.close();
    }

    // select features for the specific experiment
    private List<String> getFeaturesForAML(int experimentNum) {
        List<Object[]> features = dbUtils.select("select feature_val from amls " +
                "where experiment_num = ?", experimentNum);
        return features.stream()
                .map(entry -> entry[0].toString())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    // select features for all the existing experiments
    private List<String> getAllFeaturesFromAML() {
        List<Object[]> features = dbUtils.select("select feature_val from amls");
        return features.stream()
                .map(entry -> entry[0].toString())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private int writeAMLHeader(int orderNum, String datFileName) {
        // create file header
        writer.println("<?xml version=\"1.0\" encoding=\"windows-1251\" standalone=\"no\"?>");
        writer.println("<attributeset default_source=\"" + datFileName + "\" encoding=\"windows-1251\">");
        writer.println("<attribute name=\"" + "siteID" + "\" sourcecol=\"" + orderNum++
                + "\" valuetype=\"integer\"/>");
        return orderNum;
    }

    private int writeBody(List<String> featuresForExperiment, int orderNum) {
        for (String feature : featuresForExperiment) {
            if (feature.startsWith("text") || feature.startsWith("ngrams")) {
                // binominal values
                writer.println("<attribute name=\"" + feature + "\" sourcecol=\"" + orderNum++
                        + "\" valuetype=\"binominal\">");
                writer.println("  <value>0</value>");
                writer.println("  <value>1</value>");
                writer.println("</attribute>");
            } else if (feature.startsWith("tag")) {
                // then real values
                writer.println("<attribute name=\"" + feature + "\" sourcecol=\"" + orderNum++
                        + "\" valuetype=\"real\"/>");
            }
        }
        return orderNum;
    }

    private int writeAMLEnding(List<String> categories, int orderNum) {
        // add length (only for main text)
        writer.println("<attribute name=\"" + "fileLength" + "\" sourcecol=\""
                + orderNum++ + "\" valuetype=\"integer\"/>");

        // add category label
        for (String category : categories) {
            String categoryNum = String.valueOf((categories.indexOf(category) + 1));
            if (Integer.parseInt(categoryNum) < 10)
                categoryNum = "0" + categoryNum;

            writer.println("<attribute name=\"label_" + categoryNum + '_'
                    + category.replaceAll("[^A-Z0-9a-z_]+", "_").replaceAll("__+", "_")
                    + "\" sourcecol=\"" + orderNum++ + "\" valuetype=\"binominal\">");
            writer.println("  <value>0</value>");
            writer.println("  <value>1</value>");
            writer.println("</attribute>");
        }

        // add general label
        writer.println("<attribute name=\"label\" sourcecol=\"" + orderNum + "\" valuetype=\"nominal\">");

        for (String category : categories) {
            writer.println("  <value>" +
                    category.substring(category.indexOf("_") + 1).replaceAll("&", "&amp;") + "</value>");
        }
        writer.println("  <value>Unknown</value>");
        writer.println("</attribute>");
        writer.println("</attributeset>");

        return orderNum;
    }

    /**
     * make AML for the specific experiment
     * @param experimentNum number of the experiment
     * @param categories list of categories used
     * @param datFileName name for the dat-file
     * @throws IOException exception while working with files
     */
    public void createExperimentAML(int experimentNum, List<String> categories,
                                    String datFileName) throws IOException {

        List<String> featuresForExperiment = getFeaturesForAML(experimentNum);
        writeAML(categories, featuresForExperiment, datFileName);
    }

    /**
     * make AML for all the experiments in the db
     * @param categories categories used
     * @param datFileName name for the dat-file
     * @throws IOException exceprion while working with files
     */
    public void createUnitedAML(List<String> categories, String datFileName) throws IOException {

        List<String> featuresForExperiment = getAllFeaturesFromAML();
        writeAML(categories, featuresForExperiment, datFileName);
    }

    private void writeAML(List<String> categories, List<String> featuresForExperiment, String datFileName) {
        int orderNum = 1;

        orderNum = writeAMLHeader(orderNum, datFileName);
        orderNum = writeBody(featuresForExperiment, orderNum);
        writeAMLEnding(categories, orderNum);

        closeWriter();
        System.out.println("Success!");
    }
}
