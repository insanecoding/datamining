package downloading;

import dbUtils.DbUtils;
import org.apache.commons.io.IOUtils;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class HTMLDownloader {

    private DbUtils dbUtils;
    private int activeThreadsNumber;
    private int threadsNumber;

    public HTMLDownloader(DbUtils dbUtils, int threadsNumber) {
        this.dbUtils = dbUtils;
        this.activeThreadsNumber = 0;
        this.threadsNumber = threadsNumber;
    }

    private void atomicDownloadTask(Queue<Object[]> q) throws Exception {
        Object[] o = q.remove();
        int websiteId = (int) o[0];
        String url = (String) o[1];
        String categoryName = (String) o[2];

        System.out.println("Processing: " + url + " in category: " + categoryName);
        String html;

        URL link = new URL(url);
        URLConnection urlConn = link.openConnection();
        urlConn.setConnectTimeout(5000);
        urlConn.setReadTimeout(5000);
//                urlConn.setAllowUserInteraction(false);

        InputStreamReader is = new InputStreamReader(urlConn.getInputStream());
        html = IOUtils.toString(is);
        is.close();

        if (html.equals("")) {
            throw new Exception("Skipped " + url + " in " + categoryName + ": got empty html from page!");
        }

        Date currentDatetime = new Date();
        Timestamp datetime = new Timestamp(currentDatetime.getTime());
        System.out.println("Success! Category: " + categoryName + " " + websiteId + " " + url + " " + datetime);
        String insertQuery =
                "INSERT INTO html_data (website_id, html_contents, added_at_datetime)  VALUES (?,?,?)";
        dbUtils.query(insertQuery, websiteId, html, datetime);
    }

    private Future download(Queue<Object[]> q, ExecutorService executorService) {
        return
                executorService.submit(() -> {
                    try {
                        activeThreadsNumber++;
                        atomicDownloadTask(q);
                    } catch (NoSuchElementException ignored) {
                    } catch (Exception e) {
                        System.err.println(e.getClass().getName() + " : " + e.getMessage());
                        download(q, executorService);
                    }
                    activeThreadsNumber--;
                });
    }

    private void checkTables() {
//        if (dbUtils.tableExists("html_data2")) dbUtils.query("drop table html_data2");

        if (!dbUtils.tableExists("list_of_websites") &&
                !dbUtils.checkColumns("list_of_websites", "id", "category_id", "source_id", "website_url")) {
            System.out.println("table with websites not found");
            System.exit(-1);
        }

        if (dbUtils.tableExists("html_data")) {
            System.out.println("output table found");
        } else {
            String createQuery = "CREATE TABLE html_data " +
                    "( website_id INT NOT NULL UNIQUE REFERENCES list_of_websites(id)," +
                    "html_contents text NOT NULL, added_at_datetime TIMESTAMP ) ";
            dbUtils.query(createQuery);
            System.out.println("output table created");
        }
    }

    public void downloadHTMLs(List <String> categories, int sitesPerCategory) {

        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);

        List <Future> futures = new ArrayList<>();
        checkTables();

        String sql = "SELECT lw.id, lw.website_url, category_name " +
                "FROM list_of_websites AS lw JOIN list_of_categories AS lc " +
                "ON lw.category_id = lc.id " +
                "where category_name = ? ";

        for (String category : categories) {
            Queue<Object[]> q = new ConcurrentLinkedQueue<>(dbUtils.select(sql, category));
            IntStream.range(0, sitesPerCategory)
                    .forEach(i -> futures.add ( download(q, executorService)));
        }

        try {
            while (activeThreadsNumber > 0) {
                System.out.println(activeThreadsNumber + " processes left");
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("The end (" + activeThreadsNumber + ")");

        for(Future future : futures) {
            try {
                Object got = future.get();
                while (got != null) {
                    got = ((Future)(got)).get();
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
                executorService.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            executorService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }
}
