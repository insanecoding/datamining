package downloading;

import utils.MiscUtils;

import java.io.*;
import java.net.*;

/**
 * @author Sigwald
 * @version 1.0
 * Downloads blacklist from the Internet.
 * provide url to download from and specify where to save output
 * It's possible to use proxies, as well
 */

public class BlacklistDownloader {
    private boolean useProxy = false;
    private HttpURLConnection httpConn;
    private Proxy proxy = null;
    private int contentLength;

    /**
     * Downloader's constructor.
     * @param useProxy whether we should use proxies or no
     * @param proxyParams 2-element array of proxy parameters
     *                    (if proxy is enabled)
     *                    <code>proxyParams[0]</code> is for proxy IP
     *                    <code>proxyParams[1]</code> is for proxy port
     */
    public BlacklistDownloader(boolean useProxy, String... proxyParams) {
        this.useProxy = useProxy;
        this.contentLength = -1;
        if ((useProxy) && (proxyParams.length == 2)) {
            String proxyIP = proxyParams[0];  // set proxy IP
            int port = Integer.parseInt(proxyParams[1]);   // set port
            proxy = new Proxy(Proxy.Type.HTTP,
                    new InetSocketAddress(proxyIP, port));
        }
    }

    private boolean connect(String website) throws IOException {
        URL url = new URL(website);
        if (useProxy) {
            // use proxies if it's chosen
            httpConn = (HttpURLConnection) url.openConnection(proxy);
        } else {
            // otherwise don't use them
            httpConn = (HttpURLConnection) url.openConnection();
        }

        // checking HTTP response code
        int responseCode = httpConn.getResponseCode();
        contentLength = httpConn.getContentLength();

        return (responseCode == HttpURLConnection.HTTP_OK)
                && (contentLength != -1);
    }

    private String getFilename(String website) {
        String fileName = "";
        String disposition = httpConn.getHeaderField("Content-Disposition");
        if (disposition != null) {
            // extracts file name from header field (if present)
            int index = disposition.indexOf("filename");
            if (index > 0) {
                fileName = disposition.substring(disposition.lastIndexOf("=") + 1,
                        disposition.length());
            }
        } else {
            // otherwise extracts file name from URL
            fileName = website.substring(website.lastIndexOf("/") + 1,
                    website.length());
        }
        return fileName;
    }

    private FileOutputStream download(FileOutputStream outputStream) {
        InputStream inputStream = null;
        try {
            // opens input stream from the HTTP connection
            inputStream = httpConn.getInputStream();

            int counter = 0;
            int bytesRead;
            int totalBytesRead = 0;

            int BUFFER_SIZE = 4096;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                totalBytesRead += bytesRead;
                if ((counter % 20) == 0) {
                    System.out.print("\r");
                    double processedPercents =
                            (double) totalBytesRead / contentLength;
                    System.out.printf("Downloaded: %.2f", processedPercents * 100);
                    System.out.print("%");
                }
                System.out.print(".");  // Progress bar :)
                outputStream.write(buffer, 0, bytesRead);
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return outputStream;
    }

    // checking initial size with size of the downloaded file
    private void postDownloadCheck(String saveFilePath){
        File downloadedFile = new File(saveFilePath);
        if ((downloadedFile.length() != contentLength)) {
            System.out.println("Sizes not equal: downloaded file damaged!!!");
        }
    }

    public void downloadURL(String fromURL, String toFolder) {
        FileOutputStream fileOutputStream = null;
        try {
            if (connect(fromURL)) {
                String filename = getFilename(fromURL);
                String filePath = toFolder + File.separator + filename;
                System.out.println(filePath);
                System.out.println("The file is: " + contentLength + " bytes");
                MiscUtils.createParentDirectories(filePath);
                fileOutputStream = new FileOutputStream(filePath);
                fileOutputStream = download(fileOutputStream);
                postDownloadCheck(filePath);

                System.out.println("\r                    ");
                System.out.println("File downloaded");
            } else {
                System.out.println("Could not connect");
            }
        } catch (SocketTimeoutException e) {
            System.out.println("Connection timed out!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpConn.disconnect();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
