package schemeGenerator;

import java.io.*;
import java.util.List;

public class SchemeGenerator {

    private String baseLearnerTemplate = "";
    private String stackingTemplate = "";
    private String applyModelTemplate = "";
    private String readPerformanceTemplate = "";
    private String schemesDir = "";

    public SchemeGenerator(String amlDir, String schemesDir) {
        this.schemesDir = schemesDir;
    }

    public void setApplyModelTemplate(String applyModelTemplate) {
        this.applyModelTemplate = applyModelTemplate;
    }

    public void setBaseLearnerTemplate(String baseLearnerTemplate) {
        this.baseLearnerTemplate = baseLearnerTemplate;
    }

    public void setStackingTemplate(String stackingTemplate) {
        this.stackingTemplate = stackingTemplate;
    }

    public void setReadPerformanceTemplate (String readPerformanceTemplate) {
        this.readPerformanceTemplate = readPerformanceTemplate;
    }

    public void generateReadPerformanceScheme(String performanceFilename, String modelFilename) {
        try {
            File file = new File(readPerformanceTemplate);

            File generated = new File(schemesDir + "4)read_performance.rmp");
            File parent = generated.getParentFile();

            if (!parent.exists() && !parent.mkdirs()) {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }

            if ( !generated.createNewFile() ) {
                System.out.println("Could not create file");
            }
            PrintWriter pw = new PrintWriter(generated);

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {

                if (line.contains("!")) {
                    int lastDelimiterPosition = line.lastIndexOf("!");
                    int firstDelimiterPosition = line.indexOf("!");
                    String forReplacement = line.substring(firstDelimiterPosition, lastDelimiterPosition + 1);
                    String betweenDelimiters = line.substring(firstDelimiterPosition + 1, lastDelimiterPosition);
                    String replacement;
                    switch (betweenDelimiters) {
                        case "performance_file": {
                            replacement = performanceFilename;
                            break;
                        }
                        case "model_file": {
                            replacement = modelFilename;
                            break;
                        }
                        default: {
                            replacement = "unknown";
                        }
                    }
                    pw.println(line.replaceAll(forReplacement, replacement));
                } else {
                    pw.println(line);
                }
            }
            fileReader.close();
            pw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateBaseLearnersScheme(int categoryNum, String categoryName,
                                    String learnAML, String modelsPath, String perfPath, String mode) {
        try {
            File file = new File(baseLearnerTemplate);

            File generated = new File(schemesDir + "1)textBase_" + categoryNum + ".rmp");
            File parent = generated.getParentFile();

            if (!parent.exists() && !parent.mkdirs()) {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }

            if ( !generated.createNewFile() ) {
                System.out.println("Could not create file");
            }

            PrintWriter pw = new PrintWriter(generated);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while ((line = bufferedReader.readLine()) != null) {

                if (line.contains("!")) {
                    int lastDelimiterPosition = line.lastIndexOf("!");
                    int firstDelimiterPosition = line.indexOf("!");
                    String forReplacement = line.substring(firstDelimiterPosition, lastDelimiterPosition + 1);
                    String betweenDelimiters = line.substring(firstDelimiterPosition + 1, lastDelimiterPosition);
                    String replacement;
                    switch (betweenDelimiters) {
                        case "learnAML": {
                            replacement = learnAML;
                            break;
                        }
                        // check if is correct!!!!!
                        case "categoryNumber": {
                            if (categoryNum < 10) {
                                replacement = "0" + String.valueOf(categoryNum);
                            } else {
                                replacement = String.valueOf(categoryNum);
                            }
                            line = line.replaceAll("@mode@", mode);
                            break;
                        }
                        case "categoryNumber@categoryName": {
                            if (categoryNum < 10) {
                                replacement = "0" + String.valueOf(categoryNum) + '_' + categoryName;
                            } else {
                                replacement = String.valueOf(categoryNum) + '_' + categoryName;
                            }
                            break;
                        }
                        case "text_base_categoryNumber.mod": {
                            replacement = modelsPath + "text_base_" + categoryNum + ".mod";
                            break;
                        }
                        case "performanceFile": {
                            replacement = perfPath + "text_base_" + categoryNum + ".per";
                            break;
                        }
                        default: {
                            replacement = "unknown";
                        }
                    }
                    pw.println(line.replaceAll(forReplacement, replacement));
                } else {
                    pw.println(line);
                }
            }
            fileReader.close();
            pw.close();
            System.out.println("Text base scheme for " + categoryName + " generated!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // manual depth and gain settings
    public void generateStackingScheme(String learnAML, String stackingModPath,
                                       String stackingPerfPath, List<String> models, double minGain, int maxDepth) {
        try {
            File file = new File(stackingTemplate);

            File generated = new File(schemesDir + "2)text_Stacking.rmp");
            File parent = generated.getParentFile();

            if (!parent.exists() && !parent.mkdirs()) {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }

            if ( !generated.createNewFile() ) {
                System.out.println("Could not create file");
            }
            PrintWriter pw = new PrintWriter(generated);

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            String repeatable = "";
            int threeTimesCounter = 0;

            while ((line = bufferedReader.readLine()) != null) {

                if (line.contains("!")) {
                    int lastDelimiterPosition = line.lastIndexOf("!");
                    int firstDelimiterPosition = line.indexOf("!");
                    String forReplacement = line.substring(firstDelimiterPosition, lastDelimiterPosition + 1);
                    String betweenDelimiters = line.substring(firstDelimiterPosition + 1, lastDelimiterPosition);
                    String replacement;
                    switch (betweenDelimiters) {
                        case "learnAML": {
                            replacement = learnAML;
                            break;
                        }
                        case "text_stacking_mod": {
                            replacement = stackingModPath + "text_stacking.mod";
                            break;
                        }
                        case "text_stacking_per": {
                            replacement = stackingPerfPath + "text_stacking.per";
                            break;
                        }
                        case "minimal_gain": {
                            replacement = String.valueOf(minGain);
                            break;
                        }
                        case "maximal_depth": {
                            replacement = String.valueOf(maxDepth);
                            break;
                        }
                        default: {
                            replacement = "unknown";
                        }
                    }
                    pw.println(line.replaceAll(forReplacement, replacement));
                } else if (line.contains("BaseModelBlock")) {

                    repeatable = repeatable + line + System.getProperty("line.separator");
                    threeTimesCounter++;

                    if (threeTimesCounter == 3) {
                        String result = "";
                        String sub = repeatable.replaceAll(" No", "");

                        for (int i = 0; i < models.size(); i++) {
                            String replacement = models.get(i);   // path to the specific model
                            if (i == 0) {
                                result += sub.replaceAll("Base_Model", replacement);
                            }
                            else {
                                result += repeatable.replaceAll("Base_Model", replacement).replaceAll("No", "(" + String.valueOf(i+1) + ")" );
                            }
                        }
                        result = result.substring(0, result.length()-2);

                        pw.println(result.replaceAll("BaseModelBlock", ""));
                    }
                } else if (line.contains("PortConnection")) {
                    String result = "";

                    for ( int i = 1; i < ( models.size() + 1 ); i++ ) {

                        if (i == 1) {
                            result += line.replaceAll("Num", String.valueOf(i)).replaceAll(" No", "");
                            result += System.getProperty("line.separator");
                        }
                        else {
                            result += line.replaceAll("Num", String.valueOf(i)).replaceAll("No", "(" + String.valueOf(i) + ")" );
                            result += System.getProperty("line.separator");
                        }
                    }
                    result = result.substring(0, result.length()-2);
                    pw.println(result.replaceAll("PortConnection", ""));
                } else if (line.contains("SyncModel")) {
                    String result = "";

                    for ( int i = 1; i <= ( models.size() + 1 ); i++ ) {
                        result += line.replaceAll("Num", String.valueOf(i));
                        result += System.getProperty("line.separator");
                    }
                    result = result.substring(0, result.length()-2);
                    pw.println(result.replaceAll("SyncModel", ""));
                } else {
                    pw.println(line);
                }
            }
            fileReader.close();
            pw.close();
            System.out.println("Stacking scheme for " + models.size() + " categories generated!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // using default gain and depth settings
    public void generateDefaultStackingScheme(String learnAML, String stackingModPath,
                                              String stackingPerfPath, List<String> models) {
        generateStackingScheme(learnAML, stackingModPath, stackingPerfPath, models, 0.001, 20);
    }

    public void generateApplyModelScheme(String learnedModel, String testAML, String textPerformance) {
        try {
            File file = new File(applyModelTemplate);

            File generated = new File(schemesDir + "3)text_applyModel.rmp");
            File parent = generated.getParentFile();

            if (!parent.exists() && !parent.mkdirs()) {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }

            if ( !generated.createNewFile() ) {
                System.out.println("Could not create file");
            }
            PrintWriter pw = new PrintWriter(generated);

            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            while ((line = bufferedReader.readLine()) != null) {

                if (line.contains("!")) {
                    int lastDelimiterPosition = line.lastIndexOf("!");
                    int firstDelimiterPosition = line.indexOf("!");
                    String forReplacement = line.substring(firstDelimiterPosition, lastDelimiterPosition + 1);
                    String betweenDelimiters = line.substring(firstDelimiterPosition + 1, lastDelimiterPosition);
                    String replacement;
                    switch (betweenDelimiters) {
                        case "readModel": {
                            replacement = learnedModel;
                            break;
                        }
                        // check if is correct!!!!!
                        case "testAML": {
                            replacement = testAML;
                            break;
                        }
                        case "textPerformance": {
                            replacement = textPerformance;
                            break;
                        }
                        default: {
                            replacement = "unknown";
                        }
                    }
                    pw.println(line.replaceAll(forReplacement, replacement));
                } else {
                    pw.println(line);
                }
            }
            fileReader.close();
            pw.close();
            System.out.println("Applying text model scheme generated!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
