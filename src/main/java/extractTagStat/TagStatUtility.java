package extractTagStat;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.*;
import java.util.stream.Collectors;

class TagStatUtility {

    /**
     * calculation precision
     * if precision = 2, then the values will be rounded to 2 digits after comma
     */
    private int precision;

    /**
     * use the default precision of 2
     */
    public TagStatUtility() {
        precision = 2;
    }

    /**
     * use manual precision
     * @param precision precision value
     */
    public TagStatUtility(int precision) {
        this.precision = precision;
    }


    Map<String, SummaryStatistics> calculateTagStat(Map<String, SummaryStatistics> statistics,
                                                    List<String> allHTMLTags) {
        // calculate number of all tags in page
        int totalTags = allHTMLTags.size();

        // leave only unique elements
        Set<String> uniqueTags = new HashSet<>(allHTMLTags);

        // tags that are present in every web page - should be excluded
        List <String> tagsToSkip = Arrays.asList("html", "head", "title", "body");

        // foreach unique tag
        for (String tag : uniqueTags) {
            // if <tag> is one of: html, head, title, body - skip it
            if (tagsToSkip.contains(tag)) {
                continue;
            }

            // if the tag is not in dictionary yet
            if (!statistics.keySet().contains(tag)) {
                // create entry for it
                statistics.put(tag, new SummaryStatistics());
            }

            // calculating number of <tag> instances in 'allHTMLTags'
            double tagQuantity = Collections.frequency(allHTMLTags, tag);
            // calculate tag frequency (in percents)
            double tagStat = tagQuantity / totalTags * 100;

            // round result to desired precision
            String formatter = "%." + precision + "f";
            String result = String.format(formatter, tagStat);
            // now result is stored in double value
            double value = Double.parseDouble(result.replace(",", "."));
            // add another value
            statistics.get(tag).addValue(value);
        }
        return statistics;
    }
}

