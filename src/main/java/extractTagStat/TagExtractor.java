package extractTagStat;

import dbUtils.DbUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TagExtractor {

    private DbUtils dbUtils;

    public TagExtractor(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    public void extractTags(Map<String, List<Integer>> IDsByCategory) {
        checkTables();

        IDsByCategory.forEach((category, ids) -> {
            System.out.println(category);
            ids.forEach(id -> {
                List<Object[]> res = dbUtils.select("select html_contents\n" +
                        "from html_data\n" +
                        "where website_id = ?", id);

                if (res.size() != 0) {
                    String html = (String) res.get(0)[0];
                    // extract all tags from page
                    List<String> tags = getTagsFromPage(html);
                    // transform list to string (elements are divided with whitespaces
                    String output = tags.stream().collect(Collectors.joining(" "));

                    if (!output.equals("")) {
                        dbUtils.query("insert into tag_data (website_id, tags) values (?, ?) ", id, output);
                    }
                }
            });
        });
    }

    private void checkTables() {

        if (!dbUtils.tableExists("html_data")) {
            System.exit(-1);
        }

        // drop output table if it exists
        if (dbUtils.tableExists("tag_data")) {
            dbUtils.query("drop table tag_data");
            System.out.println("tag_data table dropped");
        }

        dbUtils.query("create table tag_data (\n" +
                "  website_id INTEGER PRIMARY KEY NOT NULL REFERENCES list_of_websites (id),\n" +
                "  tags text\n" +
                ")");
        System.out.println("tag_data table created");
    }

    /**
     * Extract all tags from page's HTML
     *
     * @param html page to process
     * @return list of html tags in page
     */
    private List<String> getTagsFromPage(String html) {
        Document doc = Jsoup.parse(html);

        // form list of all html tags from page (they have duplicates!)
        List<String> allHTMLTags = doc.getAllElements()
                .stream()
                .map(e -> e.tagName().toLowerCase())
                .collect(Collectors.toList());

        // delete '#root' element, which is actually not an html-tag
        int idx = allHTMLTags.indexOf("#root");
        allHTMLTags.remove(idx);
        return allHTMLTags;
    }

}
