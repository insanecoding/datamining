package extractTagStat;

import dbUtils.DbUtils;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import java.util.*;

public class TagDictionary {
    private DbUtils dbUtils;
    private int precision;

    public TagDictionary(DbUtils dbUtils, int precision) {
        this.dbUtils = dbUtils;
        this.precision = precision;
    }

//    private void foo(List <String> lst, String elem) {
//        double occurrencesNumber = Collections.frequency(lst, elem);
//        double totalSize = lst.size();
//        double freq = occurrencesNumber / totalSize;
//        System.out.println( elem + " " + freq);
//    }

    private Map <String, Double> prepareDictionary(Map<String, List<Integer>> IDsByCategory){
        // at the end of this method <code>statistics</code>
        // will store pairs: tag - sum of its frequencies
        // top N tags with the highest frequency will be in dictionary
        Map<String, SummaryStatistics> statistics = new HashMap<>();
        TagStatUtility tagStatUtility = new TagStatUtility(precision);
        Map <String, Double> dictionary = new HashMap<>();

        for (String category : IDsByCategory.keySet()) {
            System.out.println(category);

            List <Integer> ids = IDsByCategory.get(category);
            for (int id : ids) {
                String sql = "select tags from tag_data where website_id = ?";
                List <Object[]> res = dbUtils.select(sql, id);
                // result set size should be = 1
                if (res.size() != 0) {
                    String tagsStr = (String) res.get(0)[0];
                    // convert string with whitespaces to list
                    List<String> allHTMLTags = Arrays.asList(tagsStr.split(" "));
                    // update <code>statistics</code> map
                    statistics = tagStatUtility.calculateTagStat(statistics, allHTMLTags);
                }
            }
        }
        statistics.forEach( (K, V) -> dictionary.put(K, V.getSum()));
        return dictionary;
    }

    private void writeDictionary(Map<String, Double> dictionary) {
        dictionary.forEach( (tag,totalFreq) -> {
            String formatter = "%." + precision + "f";
            String resultStr = String.format(formatter, totalFreq);
            double newValue = Double.parseDouble(resultStr.replace(",", "."));
            dictionary.put(tag, newValue);
        });

        dictionary.entrySet().stream().sorted(Map.Entry.<String, Double>comparingByValue().reversed())
//                .limit(60)
                .forEach( entry ->
                        dbUtils.query("insert into tagDictionary (tag_name, total_frequency) " +
                        " values (?, ?) ", entry.getKey(), entry.getValue()));
    }

    private void checkTables() {
        if (dbUtils.tableExists("tagdictionary")) {
            dbUtils.query("drop table tagdictionary");
            System.out.println("Previous tagDictionary table dropped");
        }

        dbUtils.query("create table tagDictionary (\n" +
                "  tag_num SERIAL PRIMARY KEY NOT NULL,\n" +
                "  tag_name VARCHAR(300),\n" +
                "  total_frequency REAL\n" +
                ")");
    }

    public void createDictionary(Map<String, List<Integer>> IDsByCategory){
        checkTables();
        Map <String, Double> dictionary = prepareDictionary(IDsByCategory);
        writeDictionary(dictionary);
    }
}
