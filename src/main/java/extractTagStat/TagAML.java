package extractTagStat;

import dbUtils.DbUtils;

import java.util.List;

public class TagAML {

    private DbUtils dbUtils;

    public TagAML(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    public void prepareTagAML(int dictionaryWordsCount, int experimentNumber) {
        String sql = "SELECT tag_name\n" +
                "FROM tagdictionary\n" +
                "ORDER BY total_frequency desc\n" +
                "limit ? ";

        List<Object[]> res = dbUtils.select(sql, dictionaryWordsCount);
        // normally res is not empty
        if (res.size() == 0) System.exit(-1);

        final int[] counter = {0};
        res.forEach( entry -> {
            ++counter[0];
            String tag = (String)entry[0];
            String counterAsString;

            if (counter[0] < 10) counterAsString = "00" + counter[0];
            else if (counter[0] < 100) counterAsString = "0" + counter[0];
            else counterAsString = String.valueOf(counter[0]);

            tag = "tag_" + counterAsString + "_" + tag;
            String insert = "insert into amls (feature_val, experiment_num) values (?, ?)";
            dbUtils.query(insert, tag, experimentNumber);
        });
    }
}
