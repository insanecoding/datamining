package extractTagStat;

import dbUtils.DbUtils;

import java.util.*;

public class TagDAT {
    private DbUtils dbUtils;
    private int precision;

    public TagDAT(DbUtils dbUtils, int precision) {
        this.dbUtils = dbUtils;
        this.precision = precision;
    }

    public void prepareDAT(Map<String, List<Integer>> IDsByCategory,
                           int experimentNum, List <String> tagDictionary){
        for (String category : IDsByCategory.keySet()) {
            System.out.println(category);
            List <Integer> ids = IDsByCategory.get(category);
            for ( int id : ids) {
                processTags(IDsByCategory, experimentNum, tagDictionary, category, id);
            }
        }
    }

    private void processTags(Map<String, List<Integer>> IDsByCategory,
                             int experimentNum,
                             List<String> tagDictionary,
                             String category, int id) {
        String features = "";
        String sql = "select tags\n" +
                "from tag_data\n" +
                "where website_id = ?";
        List<Object[]> res = dbUtils.select(sql, id);

        // if no tags for the website then it is unknown
        if (res.size() == 0) res.get(0)[0] = "";

        String tagsStr = (String) res.get(0)[0];
        List <String> tagsInPage = Arrays.asList(tagsStr.split(" "));

        int zeroCounter = 0;
        // for each tag in the dictionary
        for (String tagFromDictionary : tagDictionary) {
            // <code>tagFromDictionary</code> is like: tag_001_div. But we need only 'div'
            String[] elements = tagFromDictionary.split("_");
            tagFromDictionary = elements[2];
            // if page contains it
            if (tagsInPage.contains(tagFromDictionary)) {
                double value = calculateStat(tagsInPage, tagFromDictionary);
                features += "\"" + value + "\" ";
            } else {
                //it may be unknown
                features += "\"0.0\" ";
                zeroCounter += 1;
            }
        }
        // delete trailing space
//        features = features.trim();
        // if all the features are zeros, then it is unknown
        boolean isUnknown = (zeroCounter == tagDictionary.size());

        String categoriesBasis = "";
        Set<String> categories = IDsByCategory.keySet();
        for (String currentCategory : categories) {
            if (currentCategory.equals(category)) {
                categoriesBasis += "\"1\" ";
            } else {
                categoriesBasis += "\"0\" ";
            }
        }
        categoriesBasis += category;

        // retrieve filename
        List<Object[]> result = dbUtils.select("SELECT filename from filenames\n" +
                "where website_id = ? ", id);
        if (result.size() == 0) System.exit(-1);
        String filename = (String) result.get(0)[0];

        dbUtils.query("insert into dataSets (website_id, website_name, features, " +
                        " fileLength, categories_basis, isUnknown, experiment_num) values (?, ?, ?, ?, ?, ?, ?)",
                id, filename, features, -1,
                categoriesBasis, isUnknown, experimentNum);
    }

    private double calculateStat(List<String> tagsInPage, String dictionaryTag) {
        // calculate tag stat
        double tagQuantity = Collections.frequency(tagsInPage, dictionaryTag);
        double tagStat = tagQuantity / tagsInPage.size() * 100;

        // round result to desired precision
        String formatter = "%." + precision + "f";
        String result = String.format(formatter, tagStat);
        // now result is stored in double value
        return Double.parseDouble(result.replace(",", "."));
    }
}
