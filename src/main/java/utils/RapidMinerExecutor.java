package utils;

import java.io.IOException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

public class RapidMinerExecutor {

    private String pathToRM;

    public RapidMinerExecutor(String pathToRM) {
        this.pathToRM = pathToRM;
    }

    public void executeProcessInRM(String pathToProcess) {
        String line = "'" + pathToRM + "'" +
                " -f" +
                " '" + pathToProcess + "'";
        CommandLine cmdLine = CommandLine.parse(line);
        DefaultExecutor executor = new DefaultExecutor();

        try {
            int exitValue = executor.execute(cmdLine);
            System.out.println(pathToProcess + " exited with " + exitValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
