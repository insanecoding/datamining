package utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.validator.routines.UrlValidator;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class MiscUtils {

    public static String checkURLBeginning(String url) {
        String result;
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            result = "http://" + url;
        } else {
            result = url;
        }
        return result;
    }

    public static boolean isValidURL(String link) {
        UrlValidator urlValidator = new UrlValidator();
        return urlValidator.isValid(link);
    }


    public static String readProperty(String fromConfig, String value) {
        String result = "";
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(fromConfig);
            prop.load(input);   // load a properties file
            result = prop.getProperty(value); // get the property value
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static String[] readPropertiesAsArray(String fromConfig, String property) {
        InputStream input = null;
        String[] output = null;
        try {
            Properties prop = new Properties();
            input = new FileInputStream(fromConfig);
            prop.load(input);
            output = prop.getProperty(property).split(",");

            // remove possible leading and trailing whitespaces
            for (int i = 0; i < output.length; i++) {
                output[i] = output[i].trim();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return output;
    }

    /**
     * Delete directory and its subdirectories
     * @param startingDir path to the directory
     * @return <code>true</code> if success, <code>false</code> in case of errors
     * @throws IOException exception with files
     */
    public static boolean deleteDirRecursively (String startingDir)
            throws IOException {
        File dirToDelete = new File(startingDir);
        if (dirToDelete.exists() && dirToDelete.isDirectory()) {
            FileUtils.deleteDirectory(dirToDelete);
            System.out.println("Clean-up done!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Create directory and all the missing preceding directories
     * For example: <code> createFullPath("1/2/3/4/5")</code>
     * will create all path: 1/2/3/4/5
     * @param path path to create
     * @return true on success, false in case of a trouble
     */
    public static boolean createFullPath(String path) {
        File files = new File(path);
        return !files.exists() && files.mkdirs();
    }


    /**
     * Create only parent directories of the specified path
     * Fpr example, <code> createParentDirectories("1/2/3/4/5")</code>
     * will create path: 1/2/3/4
     * @param filePath full path to create
     * @return true on success, false upon fail
     */
    public static boolean createParentDirectories(String filePath) {
        File newFile = new File(filePath);
        return ((!newFile.exists()) && (newFile.getParentFile().mkdirs()));
    }

    public static List<String> readTextFileToList(String fName) {
        List<String> categories = new LinkedList<>();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(fName));
            String currentCategory;
            while ((currentCategory = bf.readLine()) != null) {
                categories.add(currentCategory);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return categories;
    }

    public static String[] chooseTable(String mode) {
        String fieldToSelect = "";
        String tableToSelect = "";

        if (mode.equals("text_main")) {
            fieldToSelect = "text_contents";
            tableToSelect = "text_data";
        } else if (mode.contains("text_")){
            fieldToSelect = "text_from_tag_contents";
            tableToSelect = mode;
        } else if (mode.equals("ngrams")) {
            fieldToSelect = "ngram";
            tableToSelect = "ngrams";
        }
        return new String[]{fieldToSelect, tableToSelect};
    }
}
