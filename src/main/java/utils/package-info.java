/**
 * This package contains various supporting tools: <br>
 * - for downloading categorized websites from the Internet <br>
 * - for unpacking them from the archives <br>
 * - etc.
 */
package utils;
