package utils;

import org.apache.commons.io.FilenameUtils;
import org.rauschig.jarchivelib.ArchiverFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class UncompressTool {

    /**
     * Method for uncompressing .tar.gz or .zip files
     * @param archive path to the archive
     * @param destination path for the uncompressed contents
     * @throws IOException
     */
    private static void tarGZorZip(String archive, String destination) throws IOException {
        File fromArchive = new File (archive);
        File toFile = new File (destination);
        org.rauschig.jarchivelib.Archiver archiver = ArchiverFactory.createArchiver(fromArchive);
        archiver.extract(fromArchive, toFile);
    }

    /**
     * Method for uncompressing .gz files
     * @param fromArchive path to the archive
     * @param toFile path for the uncompressed contents
     * @throws IOException
     */
    private static void GZip(String fromArchive, String toFile) throws IOException {
        GZIPInputStream gzis = null;
        FileOutputStream out = null;

        try {
            byte[] buffer = new byte[1024];
            gzis = new GZIPInputStream(new FileInputStream(fromArchive));

            String filename = toFile + '/' + FilenameUtils.getBaseName(fromArchive);
            File d1 = new File(filename);
            MiscUtils.createFullPath(filename);

            out = new FileOutputStream(new File(d1.getAbsolutePath() +
                    '/' + FilenameUtils.getBaseName(fromArchive)));

            int len;
            while ((len = gzis.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        } finally {
            if (gzis != null) {
                try {
                    gzis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Method for uncompressing .tar.gz/ .zip/ .gz files
     * @param fromArchive path to the archive
     * @param toFile path for the uncompressed contents
     */
    public static void uncompress(String fromArchive, String toFile) {
        System.out.println("Decompressing file " + fromArchive + " now");
        try {
            if (fromArchive.endsWith(".tar.gz") || fromArchive.endsWith(".zip")) {
                tarGZorZip(fromArchive, toFile);
            } else if (!fromArchive.endsWith(".tar.gz") && fromArchive.endsWith(".gz")) {
                GZip(fromArchive, toFile);
            } else {
                System.out.println("Unknown archive type!");
            }
            System.out.println("Successfully decompressed!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uncompress all the archives inside the <code>compressed</code> folder
     * @param compressed starting folder (with all archives)
     * @param uncompressed where to store output
     */
    public static void uncompressAll(String compressed, String uncompressed) {
        File file = new File(compressed);
        File[] subFolders = file.listFiles();

        assert subFolders != null;
        for (File subFolder: subFolders) {
            String subFolderPath = subFolder.getAbsolutePath();
            if (subFolder.isFile()) {
                String folderName = subFolder.getParentFile().getName();
                String pathToSave = uncompressed + "//" + folderName;
                UncompressTool.uncompress(subFolderPath, pathToSave);
            } else {
                uncompressAll(subFolderPath, uncompressed);
            }
        }
    }
}
