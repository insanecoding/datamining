package utils;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class LanguageDetect {
    /**
     * constructor for language detector (used only once)
     * @param path path to the profiles
     */
    public LanguageDetect(String path) {
        try {
            DetectorFactory.loadProfile(path);
        } catch (LangDetectException e) {
            e.printStackTrace();
        }
    }

    /**
     * method for language detection
     * @param text text which language should be detected
     * @return language of text
     */
    public String detectLang(String text) {
        Detector detector;
        String lang = "unknown";
        try {
            detector = DetectorFactory.create();
            detector.append(text);
            lang = detector.detect();
        } catch (LangDetectException e) {
            System.out.println(e.getMessage());
        }
        return lang;
    }

    /**
     * something like 'destructor'
     * used if several profiles are loaded one-by-one, for example, 'profiles' and 'profiles.sm'
     * to avoid com.cybozu.labs.langdetect.LangDetectException: duplicate the same language profile
     */
    public void clear() {
        DetectorFactory.clear();
    }
}

