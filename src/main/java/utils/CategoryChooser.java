package utils;

import dbUtils.DbUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryChooser {

    private DbUtils dbUtils;

    public CategoryChooser(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    private int disableAllCategories() {
        int updates = 0;
        String sql = "UPDATE list_of_categories SET isused = 0";
        updates += dbUtils.query(sql);
        return updates;
    }

    private int enableCategories(List<String> categories, DbUtils dbUtils) {

        String sql = "";
        for (String category : categories) {
            sql += "'" + category + "', ";
        }
        sql = "(" + sql.substring(0, sql.length() - 2) + ")";
        int updates = 0;
        sql = "UPDATE list_of_categories SET isused = 1 " +
                "WHERE category_name in " + sql;
        updates += dbUtils.query(sql);

        return updates;
    }

    public List<String> makeChoiceOnCategories(List<String> categories) {
        disableAllCategories();
        System.out.println("All the categories set to 0");
        enableCategories(categories, dbUtils);

        List <Object[]> lst = dbUtils.select(
                "SELECT category_name, isused " +
                        "FROM list_of_categories " +
                        "where isused = 1 " +
                        "order by category_name");
        return lst
                .stream()
                .map(entry -> entry[0].toString())
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
