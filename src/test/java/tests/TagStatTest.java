package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import extractTagStat.TagDictionary;

import java.util.Arrays;
import java.util.List;

public class TagStatTest {
//    private static void foo(List<String> tags) {
//        // here will be only unique elements
//        Set<String> unique = new HashSet<>(tags);
////        System.out.println("unique:");
////        System.out.println(unique);
//
//        unique.forEach(uniqueTag -> {
//            // count occurrences of <code>uniqueTag</code> in <code>tags</code>
//            double occurrencesNumber = Collections.frequency(tags, uniqueTag);
//            // count all elements in <code>tags</code>
//            double totalSize = tags.size();
//            // calculate frequency
//            double freq = occurrencesNumber / totalSize;
//            System.out.println(uniqueTag + " " + freq);
//        });
//    }

    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);

        List<String> tagsInPage = Arrays.asList("".split(" "));
        System.out.println(tagsInPage);
    }
}
