package tests;

import utils.MiscUtils;

import java.util.Arrays;

public class MiscUtilsTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        System.out.println(String.join(" ", MiscUtils.readProperty(configName, "proxyIP"),
                MiscUtils.readProperty(configName, "port"),
                MiscUtils.readProperty(configName, "blacklists")));

        System.out.println(Arrays.toString(MiscUtils.readPropertiesAsArray(configName, "urls")));
        System.out.println(Arrays.toString(MiscUtils.readPropertiesAsArray(configName, "blacklistNames")));
        System.out.println(Arrays.toString(MiscUtils.readPropertiesAsArray(configName, "useProxy")));
    }
}
