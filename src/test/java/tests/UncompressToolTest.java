package tests;

import utils.MiscUtils;
import utils.UncompressTool;

public class UncompressToolTest {
        public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        String blacklistsCompressed = MiscUtils.readProperty(configName, "blacklists") + "downloads";
        String blacklistsUncompressed = MiscUtils.readProperty(configName, "blacklists") + "unpacked";

        UncompressTool.uncompressAll(blacklistsCompressed, blacklistsUncompressed);
    }
}
