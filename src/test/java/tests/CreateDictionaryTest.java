package tests;

import dataSplitter.DataSetTools;
import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import utils.MiscUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

class CreateDictionaryTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);

        DataSetTools dataSetTools = new DataSetTools(dbUtils);
        List<String> categories = MiscUtils.readTextFileToList("C:\\Coding\\Java\\DataMining\\src\\categories.txt");
        List <String> tables = Arrays.asList("text_data", "text_from_title", "text_meta_description",
               "text_meta_keywords" );

        tables.forEach( table -> categories.forEach(category -> {
            String field;
            if (table.equals("text_data")) field = "text_contents";
            else field = "text_from_tag_contents";

            Map<String, String> map = dataSetTools.createValidDataNames(category, table, field);
            int i = 0;
        }));

    }
}