package tests;

import utils.LanguageDetect;
import utils.MiscUtils;


class LanguageTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        String resources = MiscUtils.readProperty(configName, "resources");

        String longProfile = resources + "/profiles";
        LanguageDetect languageDetect = new LanguageDetect(longProfile);

        System.out.println(languageDetect.detectLang("This is long text for language detection"));
        System.out.println(languageDetect.detectLang("Es geht um syrische Ingenieure oder Ärzte, die sich derzeit in der Türkei aufhalten. Laut \"Spiegel\" dürfen sie das Land nicht mehr Richtung EU verlassen - offenbar auch eine Folge des Flüchtlingsabkommens."));
        System.out.println(languageDetect.detectLang("Tematem sobotnich (21.05.16) komentarzy w niemieckiej prasie jest pozbawienie immunitetu 138 deputowanych w tureckim parlamencie i skutkom polityki prezydenta Recepa Erdogana"));
        System.out.println(languageDetect.detectLang("afirma Luis Hernández Navarro en su libro, que analiza el surgimiento en México de las policías comunitarias y autodefensas"));

        languageDetect.clear();

        String smallProfile = resources + "/profiles.sm";
        languageDetect = new LanguageDetect(smallProfile);

        System.out.println(languageDetect.detectLang("Short text"));
    }
}
