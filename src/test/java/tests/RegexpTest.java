package tests;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class RegexpTest {
    public static void main(String[] args) {
        File input = new File("html.html");
        List <String> chosenTags = Arrays.asList("h1", "h2", "h3", "b", "u", "img", "a");
        String str = chosenTags.toString();
        String selectedTags = str.substring(1, str.length() - 1);

        Map <String, String> tags = new LinkedHashMap<>();
        chosenTags.forEach( chosenTag -> tags.put(chosenTag, ""));

        try {
            Document doc = Jsoup.parse(input, "UTF-8");
            Elements elements = doc.select(selectedTags);

            elements.forEach(element -> {
                // case 1: <img> tag - has alt-attribute
                if (element.tagName().equals("img") && element.hasAttr("alt")) {
                    String value = element.attr("alt");
                    String totalCollected = tags.get("img") + " " + value;
                    tags.put("img", totalCollected);
                }
                // case 2: <a> tag - has title-attribute or text between <a> </a>
                if (element.tagName().equals("a")) {
                    String result = "";
                    // if has title-attribute
                    if (element.hasAttr("title")) result += " " + element.attr("title");
                    // for text between <a> </a>
                    if (element.hasText()) result += " " + element.text();

                    if (!result.equals("")) {
                        String collected = tags.get("a") + " " + result;
                        tags.put("a", collected);
                    }
                }

                // case 3: <h1> - <h3>
                if (element.hasText()) {
                    String result = element.text();
                    String elemName = element.tagName();
                    String collected = tags.get(elemName) + " " + result;
                    tags.put(elemName, collected);
                }
            });

            tags.forEach( (K,V) -> {
                if (!V.equals("")) System.out.println(K + " = " + V);
            });

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
