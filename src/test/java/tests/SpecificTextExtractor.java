package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import extractText.TagTextExtractor;
import utils.LanguageDetect;
import utils.MiscUtils;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SpecificTextExtractor {

    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        String dictionary = MiscUtils.readProperty(configName, "resources") + "profiles.sm";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);
        List <String> categories = MiscUtils.readTextFileToList("C:\\Coding\\Java\\DataMining\\src\\categories.txt");

        LanguageDetect languageDetect = new LanguageDetect(dictionary);
        TagTextExtractor tagTextExtractor = new TagTextExtractor(dbUtils, languageDetect);
        tagTextExtractor.extractText(categories);

    }
}
