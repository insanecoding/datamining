package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import downloading.HTMLDownloader;

import java.util.Arrays;
import java.util.List;

public class DownloadHTMLTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);
        int threadsNumber = 1000;
        HTMLDownloader HTMLDownloader = new HTMLDownloader(dbUtils, threadsNumber);

        List<String> categories = Arrays.asList("porn", "shopping", "sports", "travel", "religion");
        int sitesPerCategory = 1000;
        HTMLDownloader.downloadHTMLs(categories, sitesPerCategory);
    }
}
