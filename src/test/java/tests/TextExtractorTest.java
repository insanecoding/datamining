package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import extractText.MainTextExtractor;
import extractText.TagTextExtractor;
import utils.LanguageDetect;
import utils.MiscUtils;

import java.util.Arrays;
import java.util.List;

public class TextExtractorTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);

        String resources = MiscUtils.readProperty(configName, "resources");
        String profilesLong = resources + "profiles";
        LanguageDetect languageDetectL = new LanguageDetect(profilesLong);

        String profilesShort = resources + "profiles.sm";
        LanguageDetect languageDetectS = new LanguageDetect(profilesShort);

        MainTextExtractor mainTextExtractor = new MainTextExtractor(dbUtils, languageDetectL);
        List<String> categories = Arrays.asList("medical", "alcohol", "religion", "adult",
                "news", "hunting", "forum", "chat", "gamble");
        mainTextExtractor.extractText(categories);

        TagTextExtractor tagTextExtractor = new TagTextExtractor(dbUtils, languageDetectS);
        tagTextExtractor.extractText(categories);
    }
}
