package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import importBlacklist.DefaultBlacklist;

public class ImportBlacklistTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);
        DefaultBlacklist shallaBlacklist = new DefaultBlacklist("shalla", "http://www.shalla.com",
                "C:\\Coding\\Java\\Data Mining Tools\\DataMiningTools\\Test\\ShallaSecurityBlacklist\\",
                dbUtils, "rewrite");
        shallaBlacklist.addBlacklist();
        DefaultBlacklist urlBlacklist = new DefaultBlacklist("ubl", "http://urlblacklist",
                "C:\\Coding\\Java\\Data Mining Tools\\DataMiningTools\\Test\\URLBlacklist", dbUtils, "add");
        urlBlacklist.addBlacklist();
    }
}
