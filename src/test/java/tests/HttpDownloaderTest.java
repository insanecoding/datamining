package tests;

import downloading.BlacklistDownloader;
import utils.MiscUtils;

public class HttpDownloaderTest {
    public static void main(String[] args) {

        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        String proxyIP = MiscUtils.readProperty(configName, "proxyIP");
        String port = MiscUtils.readProperty(configName, "port");
        String saveTo = MiscUtils.readProperty(configName, "blacklists") + "downloads";
        String[] urls = MiscUtils.readPropertiesAsArray(configName, "urls");
        String[] blacklistNames = MiscUtils.readPropertiesAsArray(configName, "blacklistNames");
        String[] useProxy = MiscUtils.readPropertiesAsArray(configName, "useProxy");

        for (int i = 0; i < 4; i++) {
            BlacklistDownloader blacklistDownloader;
            // if using proxy
            if (Boolean.parseBoolean(useProxy[i])) {
                blacklistDownloader = new BlacklistDownloader(Boolean.parseBoolean(useProxy[i]), proxyIP, port);
            } else {    // otherwise
                blacklistDownloader = new BlacklistDownloader(Boolean.parseBoolean(useProxy[i]));
            }
            blacklistDownloader.downloadURL(urls[i], saveTo + blacklistNames[i]);
        }
    }
}
