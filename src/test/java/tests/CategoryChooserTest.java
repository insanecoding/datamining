package tests;

import dbUtils.DbUtils;
import dbUtils.DbUtilsFactory;
import utils.CategoryChooser;
import utils.MiscUtils;

import java.util.List;

public class CategoryChooserTest {
    public static void main(String[] args) {
        String configName = System.getProperty("user.dir") + "\\src\\config.properties";
        String chosenCategories = System.getProperty("user.dir") + "\\src\\categories.txt";
        List<String> categories = MiscUtils.readTextFileToList(chosenCategories);

        DbUtils dbUtils = DbUtilsFactory.createDbUtils(configName);
        CategoryChooser categoryChooser = new CategoryChooser(dbUtils);
        List <String> result = categoryChooser.makeChoiceOnCategories(categories);
        System.out.println("Chosen categories: ");
        System.out.println(result);
    }
}
